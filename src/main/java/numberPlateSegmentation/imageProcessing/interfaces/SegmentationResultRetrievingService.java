package numberPlateSegmentation.imageProcessing.interfaces;

import client.ResultTO;
import org.opencv.core.Mat;
import org.opencv.core.Rect;

import java.awt.image.BufferedImage;

/**
 * Created by Wojciech on 31-Oct-15.
 */
public interface SegmentationResultRetrievingService {
    ResultTO segmentSigns(BufferedImage buffImage, Rect regionOfInterest);
}
