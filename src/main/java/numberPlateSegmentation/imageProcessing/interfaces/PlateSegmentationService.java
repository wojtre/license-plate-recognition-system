package numberPlateSegmentation.imageProcessing.interfaces;

import org.opencv.core.Mat;
import org.opencv.core.Rect;

import java.util.List;

/**
 * Created by Wojciech on 06-Oct-15.
 */
public interface PlateSegmentationService {
    List<Rect> segmentPlatesFromImage(Mat image);
}
