package numberPlateSegmentation.imageProcessing.interfaces;

import org.opencv.core.Mat;

/**
 * Created by Wojciech on 01-Nov-15.
 */
public interface PlateTruncationService {
    Mat truncatePlate(Mat plate);
}
