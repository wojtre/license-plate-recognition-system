package numberPlateSegmentation.imageProcessing.interfaces;

import org.opencv.core.Mat;

import java.util.List;

/**
 * Created by Wojciech on 06-Oct-15.
 */
public interface SignsSegmentationService {
    List<Mat> separateSigns(Mat plateToSegmentation);
}
