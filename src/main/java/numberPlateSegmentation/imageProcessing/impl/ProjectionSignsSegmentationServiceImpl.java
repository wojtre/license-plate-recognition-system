package numberPlateSegmentation.imageProcessing.impl;

import numberPlateSegmentation.imageProcessing.interfaces.PlateTruncationService;
import numberPlateSegmentation.imageProcessing.interfaces.ProjectionService;
import numberPlateSegmentation.imageProcessing.interfaces.SignsSegmentationService;
import org.opencv.core.Mat;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;
import utils.ImageUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

/**
 * Created by Wojciech on 06-Oct-15.
 */
public class ProjectionSignsSegmentationServiceImpl implements SignsSegmentationService {
    private static final int MIN_SIGNS_WIDTH = 10;
    private ProjectionService projectionService = new ProjectionServiceImpl();
    private PlateTruncationService plateTruncationService = new PlateTruncationServiceImpl();


    @Override
    public List<Mat> separateSigns(Mat plateImage) {
        ImageUtils.resizeImage(plateImage, 200);
        plateImage = plateTruncationService.truncatePlate(plateImage);
        prepareImage(plateImage);
        plateImage.size();
        List<Integer> placesOfPartition = projectionService.findPlacesOfPartition(plateImage);
        List<SignValidator> separatedSignValidators = extractSignsFromPlate(placesOfPartition, plateImage);
        truncateSigns(separatedSignValidators);
        removeNotValidateSigns(separatedSignValidators, plateImage.size());
        ArrayList<Mat> signs = new ArrayList<>();
        for (int i = 0; i < separatedSignValidators.size(); i++) {
            Mat signImageInMat = separatedSignValidators.get(i).getSignImageInMat();
            signs.add(signImageInMat);
        }
        return signs;
    }

    private List<SignValidator> extractSignsFromPlate(List<Integer> placesOfPartition, Mat plateImage) {
        Collections.sort(placesOfPartition);
        Iterator<Integer> it = placesOfPartition.iterator();
        List<SignValidator> separatedSignValidators = new ArrayList<>();
        Integer left = 0, right = 0;
        while (it.hasNext()) {
            right = it.next();
            if (isWidthEnought(left, right)) {
                separatedSignValidators.add(new SignValidator(cutArea(left, right, 0,
                        plateImage.rows(), plateImage)));
            }
            left = right;
        }
        if (isWidthEnought(right, plateImage.cols()-1)) {
            separatedSignValidators.add(new SignValidator(cutArea(right, plateImage.cols() - 1, 0, plateImage.rows(), plateImage)));
        }
        return separatedSignValidators;
    }

    private boolean isWidthEnought(Integer left, Integer right) {
        return right - left > MIN_SIGNS_WIDTH;
    }

    private void truncateSigns(List<SignValidator> separatedSignValidators) {
        for (int i = 0; i < separatedSignValidators.size(); i++) {
            separatedSignValidators.get(i).truncateSign();
        }
    }

    private void removeNotValidateSigns(List<SignValidator> separatedSignValidators, Size plateSize) {
        Iterator<SignValidator> iter = separatedSignValidators.iterator();
        while (iter.hasNext()) {
            if (!iter.next().isValid(plateSize)) {
                iter.remove();
            }
        }
    }

    private Mat cutArea(int left, int right, int up, int down, Mat plateImage) {
        if (left >= right) {
            return null;
        }
        Mat newImage = new Mat(down - up, right - left, plateImage.type());
        plateImage.submat(up, down, left, right).copyTo(newImage);
        return newImage;
    }

    private void prepareImage(Mat plateImage) {
        Imgproc.adaptiveThreshold(plateImage, plateImage, 255,
                Imgproc.ADAPTIVE_THRESH_MEAN_C, Imgproc.THRESH_BINARY,
                1 + (int) (plateImage.cols() / 16.0) * 2, 1);
    }

    // SPRING INJECTIONS

    public void setProjectionService(ProjectionService projectionService) {
        this.projectionService = projectionService;
    }

    public void setPlateTruncationService(PlateTruncationService plateTruncationService) {
        this.plateTruncationService = plateTruncationService;
    }
}
