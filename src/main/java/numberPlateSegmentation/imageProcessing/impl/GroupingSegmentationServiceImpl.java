package numberPlateSegmentation.imageProcessing.impl;

import numberPlateSegmentation.imageProcessing.interfaces.SignsSegmentationService;
import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;
import utils.ImageUtils;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;


/**
 * Created by Wojciech on 05-Nov-15.
 */
public class GroupingSegmentationServiceImpl implements SignsSegmentationService {

    private static final double MAX_RATIO = 1.4;
    private static final double MIN_RATIO = 0.2;
    private static final double HEIGHT_RANGE = 0.4;
    private static final double CENTRE_RANGE_MAX = 0.3;
    private static final int MAX_SIGNS = 8;
    private static final int MIN_SIGNS = 5;
    private static final double CLOSE_RATIO = 2;
    private static final int MIN_SIGN_WIDTH = 2;
    private static final int MIN_SIGN_HEIGHT = 8;
    private List<MatOfPoint> contours = new ArrayList<>();
    private static final double IMAGE_WIDTH = 200;

    @Override
    public List<Mat> separateSigns(Mat plateToSegmentation) {
        prepareImage(plateToSegmentation);
        Mat inverted = ImageUtils.invertColours(plateToSegmentation);
        List<IndexedRect> imageContours = findImageContours(inverted);
        removeNotValidContours(imageContours);
        List<List<IndexedRect>> groups = groupContours(imageContours);
        drawGroups(plateToSegmentation, groups);
        System.out.println("Groups: " + groups.size());
        removeNotValidGroups(groups);
        if (groups.size() < 1) {
            return null;
        }
        List<Mat> signs = getSigns(groups.get(0), plateToSegmentation);
        return signs;
    }

    private void drawGroups(Mat plateToSegmentation, List<List<IndexedRect>> groups) {
        Mat plateWithGroups = new Mat();
        plateToSegmentation.copyTo(plateWithGroups);
        Imgproc.cvtColor(plateWithGroups, plateWithGroups, Imgproc.COLOR_GRAY2BGR);

        for(int i = 0; i< groups.size();i++){
            plateWithGroups=drawRectanglesWithDifferentColours(plateWithGroups, groups.get(i), i%4);

        }
    }

    private void resizeImage(Mat imageToResize, double width) {
        double ratio = countRatioOfImage(imageToResize);
        double height = ratio * width;
        Imgproc.resize(imageToResize, imageToResize, new Size(width, height));
    }

    private double countRatioOfImage(final Mat image) {
        return image.height() / (double) image.width();
    }


    private List<Mat> getSigns(List<IndexedRect> rects, Mat image) {
        ArrayList<Mat> signs = new ArrayList<>();
        for (IndexedRect rect : rects) {
            Mat mat = new Mat();
            image.copyTo(mat);
            signs.add(leaveOnlyContourArea(rect, mat));
        }
        return signs;
    }

    private Mat leaveOnlyContourArea(IndexedRect rect, Mat plate) {
        Mat cont = new Mat(plate.size(), plate.type(),
                new Scalar(0));
        Imgproc.drawContours(cont, contours, rect.getIndex(), new Scalar(255),
                -1);
        Core.subtract(cont, plate, plate);
        plate = returnPlateWithInvertedColor(plate);
        return plate.submat(rect.getRect());
    }

    private Mat returnPlateWithInvertedColor(Mat plate) {
        Mat invert = new Mat(plate.rows(), plate.cols(),
                plate.type(), new Scalar(255));
        Mat copyMat = new Mat(plate.rows(), plate.cols(),
                plate.type());
        Core.subtract(invert, plate, copyMat);
        return copyMat;
    }

    private void removeNotValidGroups(List<List<IndexedRect>> groups) {
        Iterator<List<IndexedRect>> iterator = groups.iterator();
        while (iterator.hasNext()) {
            if (!isValidGroup(iterator.next())) {
                iterator.remove();
            }
        }
    }

    private boolean isValidGroup(List<IndexedRect> group) {
        return hasProperSize(group);
    }

    private boolean hasProperSize(List<IndexedRect> group) {
        return MIN_SIGNS <= group.size() && group.size() <= MAX_SIGNS;
    }

    private List<List<IndexedRect>> groupContours(List<IndexedRect> rects) {
        sortRects(rects);
        ArrayList<List<IndexedRect>> groups = new ArrayList<>();
        boolean addedToGroup = false;
        IndexedRect firstRect = rects.get(0);
        addRectToNewGroup(groups, firstRect);
        for (int rectNr = 1; rectNr < rects.size(); rectNr++) {
            for (int i = 0; i < groups.size(); i++) {
                if (belongsToGroup(groups.get(i), rects.get(rectNr))) {
                    groups.get(i).add(rects.get(rectNr));
                    addedToGroup = true;
                    break;
                }
            }
            if (!addedToGroup) {
                addRectToNewGroup(groups, rects.get(rectNr));
            }
            addedToGroup = false;
        }
        return groups;
    }

    private void sortRects(List<IndexedRect> rects) {
        rects.sort((o1, o2) -> {
            if (o1.getRect().x == o2.getRect().x) {
                return 0;
            }
            return o1.getRect().x > o2.getRect().x ? 1 : -1;
        });
    }

    private boolean belongsToGroup(List<IndexedRect> group, IndexedRect rectToCompare) {
        if (group.size() < 1) {
            return true;
        }
        return hasSimilarHeight(group.get(0).getRect(), rectToCompare.getRect())
                && hasSimilarCentre(group.get(0).getRect(), rectToCompare.getRect())
                && isCloseEnoughtToGroupAndNotInside(group, rectToCompare);
    }

    private void addRectToNewGroup(ArrayList<List<IndexedRect>> groups, IndexedRect firstRect) {
        ArrayList<IndexedRect> group = new ArrayList<>();
        groups.add(group);
        group.add(firstRect);
    }

    private boolean isCloseEnoughtToGroupAndNotInside(List<IndexedRect> group, IndexedRect rectToCompare) {
        Iterator<IndexedRect> iterator = group.iterator();
        boolean isCloseEnought = false;
        while (iterator.hasNext()) {
            IndexedRect rect = iterator.next();
            if (isInside(rectToCompare, rect) || isInside(rect, rectToCompare)) {
                if (rect.getRect().width < rectToCompare.getRect().width) {
                    iterator.remove();
                    group.add(rectToCompare);
                }
                return false;
            } else if (isCloseEnought(rect.getRect(), rectToCompare.getRect())) {
                isCloseEnought = true;
            }
        }
        return isCloseEnought;
    }

    private boolean isInside(IndexedRect insideRect, IndexedRect rect) {
        return rect.getRect().contains(insideRect.getRect().br());
    }

    private boolean isCloseEnought(Rect rect, Rect rectToCompare) {
        double ratio = Math.abs(countCentreWidth(rect) - countCentreWidth(rectToCompare)) / rect.height;
        return ratio < CLOSE_RATIO;
    }

    private boolean hasSimilarCentre(Rect rect, Rect rectToCompare) {
        double ratio = Math.abs((countCentreHeight(rect) - countCentreHeight(rectToCompare))) / (double) rect.height;
        return ratio < CENTRE_RANGE_MAX;
    }

    private double countCentreWidth(Rect rect) {
        return rect.x + rect.width / 2.0;
    }

    private double countCentreHeight(Rect rect) {
        return rect.y + rect.height / 2.0;
    }

    private boolean hasSimilarHeight(Rect rect, Rect rectToCompare) {
        double ratio = rect.height / (double) rectToCompare.height;
        return 1 - HEIGHT_RANGE < ratio && ratio < 1 + HEIGHT_RANGE;
    }

    private Mat drawRectangles(Mat image, List<IndexedRect> rects) {
        Mat imageWithDrawedRects = new Mat();
        Imgproc.cvtColor(image, imageWithDrawedRects, Imgproc.COLOR_GRAY2BGR);
        for (int i = 0; i < rects.size(); i++) {
            IndexedRect rect = rects.get(i);
            Core.rectangle(imageWithDrawedRects, rect.getRect().tl(), rect.getRect().br(), new Scalar(0, 0, 255, 255), 1);
        }
        return imageWithDrawedRects;
    }

    private Mat drawRectanglesWithDifferentColours(Mat image, List<IndexedRect> rects, int colour) {
        Mat imageWithDrawedRects = new Mat();
        image.copyTo(imageWithDrawedRects);
        for (int i = 0; i < rects.size(); i++) {
            IndexedRect rect = rects.get(i);
            Scalar scalar;
            switch (colour) {
                case 0:
                    scalar = new Scalar(0, 0, 255, 255);
                    break;
                case 1:
                    scalar = new Scalar(255, 0, 0, 120);
                    break;
                case 2:
                    scalar = new Scalar(0, 255, 0, 255);
                    break;
                case 3:
                    scalar = new Scalar(125,125, 0, 255);
                    break;
                default:
                    scalar = new Scalar(0, 0, 255, 255);
                    break;

            }
            Core.rectangle(imageWithDrawedRects, rect.getRect().tl(), rect.getRect().br(), scalar, 2);
        }
        return imageWithDrawedRects;
    }

    private void removeNotValidContours(List<IndexedRect> rects) {
        Iterator<IndexedRect> iterator = rects.iterator();
        while (iterator.hasNext()) {
            IndexedRect next = iterator.next();
            if (!(hasProperRatio(next) && hasMinimalSize(next))) {
                iterator.remove();
            }
        }
    }

    private boolean hasMinimalSize(IndexedRect sign) {
        return sign.getRect().height > MIN_SIGN_HEIGHT && sign.getRect().width > MIN_SIGN_WIDTH;
    }

    private boolean hasProperRatio(IndexedRect sign) {
        double ratio = sign.getRect().width / (double) sign.getRect().height;
        return MIN_RATIO < ratio && ratio < MAX_RATIO;
    }

    private List<IndexedRect> findImageContours(Mat image) {
        Mat imageWithContours = new Mat();
        image.copyTo(imageWithContours);
        contours.clear();
        Imgproc.findContours(imageWithContours, contours, new Mat(), Imgproc.RETR_CCOMP,
                Imgproc.CHAIN_APPROX_NONE);
        ArrayList<IndexedRect> rects = new ArrayList<>();
        for (int i = 0; i < contours.size(); i++) {
            rects.add(new IndexedRect(i, Imgproc.boundingRect(contours.get(i))));
        }
        return rects;
    }

    private void prepareImage(Mat plateImage) {
        resizeImage(plateImage, IMAGE_WIDTH);
        Imgproc.adaptiveThreshold(plateImage, plateImage, 255,
                Imgproc.ADAPTIVE_THRESH_MEAN_C, Imgproc.THRESH_BINARY,
                1 + (int) (plateImage.cols() / 12.0) * 2, 1);
    }

    private class IndexedRect {
        private int index;
        private Rect rect;

        public IndexedRect(int index, Rect rect) {
            this.index = index;
            this.rect = rect;
        }

        public int getIndex() {
            return index;
        }

        public void setIndex(int index) {
            this.index = index;
        }

        public Rect getRect() {
            return rect;
        }

        public void setRect(Rect rect) {
            this.rect = rect;
        }
    }
}