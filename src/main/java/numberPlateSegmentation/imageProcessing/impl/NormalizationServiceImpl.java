package numberPlateSegmentation.imageProcessing.impl;

import numberPlateSegmentation.imageProcessing.interfaces.NormalizationService;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.Point;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;
import utils.ImageUtils;

import static org.opencv.core.Core.line;


/**
 * Created by Wojciech on 22-Oct-15.
 */
public class NormalizationServiceImpl implements NormalizationService {
    private static final double ACC_TRESHOLD_PROPORTION = 0.7;
    private static final double MIN_LINE_SIZE_PROPORTION = 0.7;
    private static final int THRESH = 50;
    private static final double MIN_ANGLE_FOR_SHEAR = 0.5;
    private static final int MAX_GAP = 5;


    @Override
    public Mat normalizePlate(Mat plate) {
        Mat lines = HoughTransformationForHorizontalLines(plate);
        double averageAngle = countAverageAngle(lines);
        System.out.print(averageAngle * 180 / Math.PI);

        drawHoughTransformLines(lines, plate);
        Mat shearTransformation = null;
        if (Math.abs(averageAngle * 180 / Math.PI) > MIN_ANGLE_FOR_SHEAR) {
            shearTransformation = shearTransformation(averageAngle, plate);
        }
        return shearTransformation != null ? shearTransformation : plate;
    }

    // angle in radian
    private Mat shearTransformation(double angle, Mat image) {
        Mat dst = new Mat();
        Mat transformationMatrix = getShearTransformationMatrix(angle);
        Imgproc.warpAffine(image, dst, transformationMatrix, image.size());
        return dst;
    }

    private Mat getShearTransformationMatrix(double angle) {
        Mat transformationMatrix = new Mat(2, 3, CvType.CV_64F, new Scalar(0.0));
        transformationMatrix.put(0, 0, 1.0);
        transformationMatrix.put(1, 1, 1.0);
        transformationMatrix.put(1, 0, -Math.tan(angle));
        return transformationMatrix;
    }

    private double countAverageAngle(Mat lines) {
        double sum = 0;
        int nr = 0;
        double angle = 0;
        for (int i = 0; i < lines.width(); i++) {
            angle = countSingleAngle(lines, i);
            System.out.println(angle * 180 / Math.PI);
            if (angle < Math.PI / 4 && angle > -Math.PI / 4 && Double.NaN != angle) {
                sum += angle;
                nr++;
            }
        }
        if (nr == 0) {
            return 0;
        }
        return sum / (double) nr;
    }

    private double countSingleAngle(Mat lines, int lineNr) {
        double[] vec = lines.get(0, lineNr);
        double x1 = vec[0],
                y1 = vec[1],
                x2 = vec[2],
                y2 = vec[3];
        return Math.atan2(y2 - y1, x2 - x1);
    }

    private Mat HoughTransformationForHorizontalLines(Mat image) {
        Mat lines = new Mat();
        Mat imageForTransformation = new Mat();
        image.copyTo(imageForTransformation);
        int threshold = (int) (imageForTransformation.cols() * ACC_TRESHOLD_PROPORTION);
        int minLineSize = (int) (imageForTransformation.cols() * MIN_LINE_SIZE_PROPORTION);
        Imgproc.equalizeHist(imageForTransformation, imageForTransformation);
        imageForTransformation = ImageUtils.detectHorizontalEdges(imageForTransformation);
        Imgproc.threshold(imageForTransformation, imageForTransformation, THRESH, 255, Imgproc.THRESH_BINARY);
        Imgproc.HoughLinesP(imageForTransformation, lines, 1, Math.PI / 180, threshold, minLineSize, MAX_GAP);
        return lines;
    }

    private Mat HoughTransformationForVerticalLines(Mat image) {
        Mat lines = new Mat();
        Mat imageForTransformation = new Mat();
        image.copyTo(imageForTransformation);
        int threshold = (int) (imageForTransformation.rows() * 0.5);
        int minLineSize = (int) (imageForTransformation.rows() * 0.75);
        int lineGap = 2;
        Imgproc.equalizeHist(imageForTransformation, imageForTransformation);
        imageForTransformation = ImageUtils.detectVerticalEdges(imageForTransformation);
        Imgproc.threshold(imageForTransformation, imageForTransformation, THRESH, 255, Imgproc.THRESH_BINARY);
        Imgproc.HoughLinesP(imageForTransformation, lines, 1, Math.PI / 180, threshold, minLineSize, lineGap);
        return lines;
    }

    private Mat drawHoughTransformLines(Mat lines, Mat image) {
        Mat imageWithDrawedLines = new Mat();
        Imgproc.cvtColor(image, imageWithDrawedLines, Imgproc.COLOR_GRAY2BGR);
        if (lines.size().height != 1.0) {
            return image;
        }
        for (int i = 0; i < lines.size().width; i++) {
            printLine(lines, imageWithDrawedLines, i);
        }
        return imageWithDrawedLines;
    }

    private void resizeImage(Mat imageToResize, double width) {
        double ratio = countRatioOfImage(imageToResize);
        double height = ratio * width;
        Imgproc.resize(imageToResize, imageToResize, new Size(width, height));
    }
    private double countRatioOfImage(final Mat image) {
        return image.height() / (double) image.width();
    }


    private void printLine(Mat lines, Mat imageWithDrawedLines, int i) {
        double[] vec = lines.get(0, i);
        double x1 = vec[0],
                y1 = vec[1],
                x2 = vec[2],
                y2 = vec[3];
        Point start = new Point(x1, y1);
        Point end = new Point(x2, y2);
        line(imageWithDrawedLines, start, end, new Scalar(0, 0, 255), 1);
    }
}
