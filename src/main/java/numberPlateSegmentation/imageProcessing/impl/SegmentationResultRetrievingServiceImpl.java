package numberPlateSegmentation.imageProcessing.impl;

import client.ResultTO;
import numberPlateSegmentation.imageProcessing.interfaces.NormalizationService;
import numberPlateSegmentation.imageProcessing.interfaces.PlateSegmentationService;
import numberPlateSegmentation.imageProcessing.interfaces.SegmentationResultRetrievingService;
import numberPlateSegmentation.imageProcessing.interfaces.SignsSegmentationService;
import org.opencv.core.Mat;
import org.opencv.core.Rect;
import org.opencv.core.Size;
import org.opencv.highgui.Highgui;
import utils.ImageUtils;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by Wojciech on 31-Oct-15.
 */
public class SegmentationResultRetrievingServiceImpl implements SegmentationResultRetrievingService {
    private PlateSegmentationService plateSegmentationService = new PlateSegmentationServiceImpl();
    private NormalizationService normalizationService = new NormalizationServiceImpl();
    private SignsSegmentationService signsSegmentationService = new GroupingSegmentationServiceImpl();

    private static final double SIZE_HORIZONTAL_EXPAND_RATIO = 0.01;
    private static final double SIZE_VERTICAL_EXPAND_RATIO = 0.25;

    @Override
    public ResultTO segmentSigns(BufferedImage buffImage, Rect regionOfInterest) {
        Mat image = ImageUtils.bufferedImageToMat(buffImage);
        ResultTO resultTO = new ResultTO();
        List<Rect> foundPlates = plateSegmentationService.segmentPlatesFromImage(image);
        removePlateNotFromROI(foundPlates, regionOfInterest);
        resultTO.setFoundPlates(foundPlates);
        List<Mat> normalizedPlates = new ArrayList<>();
        List<List<Mat>> allSigns = new ArrayList<>();
        for (Mat plate : getAllRectsFromImage(image, resultTO.getFoundPlates())) {
            Mat normalizePlate = normalizationService.normalizePlate(plate);
            normalizedPlates.add(normalizePlate);
        }
        for (Mat normalizedPlate : normalizedPlates) {
            List<Mat> separateSigns = signsSegmentationService.separateSigns(normalizedPlate);
            allSigns.add(separateSigns);
        }
        resultTO.setNormalizedPlates(normalizedPlates);
        resultTO.setSegmentedSigns(allSigns);
        return resultTO;
    }

    private void removePlateNotFromROI(List<Rect> rects, Rect regionOfInterest) {
        if (regionOfInterest == null) {
            return;
        }
        Iterator<Rect> iterator = rects.iterator();
        while (iterator.hasNext()) {
            Rect next = iterator.next();
            if (!(regionOfInterest.contains(next.br()) && regionOfInterest.contains(next.tl()))) {
                iterator.remove();
            }
        }
    }

    private List<Mat> getAllRectsFromImage(Mat image, List<Rect> rects) {
        List<Mat> cutRects = new ArrayList<>();
        for (int i = 0; i < rects.size(); i++) {
            Rect smallRect = rects.get(i);
            cutRects.add(image.submat(expandRect(smallRect, image.size())));
        }
        return cutRects;
    }

    private Rect expandRect(Rect smallRect, Size imageSize) {
        double[] points = new double[4];
        double newX = smallRect.x - smallRect.width * SIZE_HORIZONTAL_EXPAND_RATIO;
        double newY = smallRect.y - smallRect.height * SIZE_VERTICAL_EXPAND_RATIO;
        points[0] = Math.max(newX, 0);
        points[1] = Math.max(newY, 0);
        points[2] = Math.min(smallRect.width * (1 + SIZE_HORIZONTAL_EXPAND_RATIO * 2), imageSize.width - 1 - points[0]);
        points[3] = Math.min(smallRect.height * (1 + SIZE_VERTICAL_EXPAND_RATIO * 2), imageSize.height - 1 - points[1]);
        return new Rect(points);
    }

    // Spring injections
    public void setPlateSegmentationService(PlateSegmentationService plateSegmentationService) {
        this.plateSegmentationService = plateSegmentationService;
    }

    public void setNormalizationService(NormalizationService normalizationService) {
        this.normalizationService = normalizationService;
    }

    public void setSignsSegmentationService(SignsSegmentationService signsSegmentationService) {
        this.signsSegmentationService = signsSegmentationService;
    }
}
