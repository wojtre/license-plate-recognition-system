package numberPlateSegmentation.imageProcessing.impl;

import numberPlateSegmentation.imageProcessing.interfaces.PlateSegmentationService;
import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;
import utils.ImageUtils;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by Wojciech on 18-Oct-15.
 */
public class PlateSegmentationServiceImpl implements PlateSegmentationService {

    private static final double HIGHEST_RATIO = 0.5;
    private static final double LOWEST_RATIO = 0.12;
    private static final int THRESH = 230;
    private static final int MIN_EDGES_NR = 14;
    private static final int MAX_HEIGHT = 50;
    private static final int MIN_HEIGHT = 5;
    private static final int MAX_WIDTH = 100;
    private static final int MIN_WIDTH = 20;
    private static final int MAX_EDGES_NR = 30;


    @Override
    public List<Rect> segmentPlatesFromImage(Mat image) {
        Mat equalizedImage = new Mat();
        Imgproc.equalizeHist(image, equalizedImage);
        Mat binarizedImageWithEdges = getBinarizedImageWithEdges(equalizedImage);
        Mat imageWithProbablyPlateArea = localizeProbablyPlateArea(binarizedImageWithEdges);
        List<Rect> rectanglesOfPossiblePlateAreas = getRectanglesOfPossiblePlateAreas(imageWithProbablyPlateArea);
        removeNotValidPlates(binarizedImageWithEdges, rectanglesOfPossiblePlateAreas);
        return rectanglesOfPossiblePlateAreas;
//        return getAllRectsFromImage(equalizedImage, rectanglesOfPossiblePlateAreas);
    }

    private void removeNotValidPlates(Mat binarizedImageWithEdges, List<Rect> rectanglesOfPossiblePlateAreas) {
        removePlateAreasWithNoProperRatio(rectanglesOfPossiblePlateAreas);
        removePlateAreasWithNoProperSize(rectanglesOfPossiblePlateAreas);
        removePlateAreasWithNoProperEdgesNumber(rectanglesOfPossiblePlateAreas, binarizedImageWithEdges);
    }

    private void resizeImage(Mat imageToResize, double width) {
        double ratio = countRatioOfImage(imageToResize);
        double height = ratio * width;
        Imgproc.resize(imageToResize, imageToResize, new Size(width, height));
    }

    private double countRatioOfImage(final Mat image) {
        return image.height() / (double) image.width();
    }

    private Mat drawRectangles(Mat image, List<Rect> rects) {
        Mat imageWithDrawedRects = new Mat();
        Imgproc.cvtColor(image, imageWithDrawedRects, Imgproc.COLOR_GRAY2BGR);
        for (int i = 0; i < rects.size(); i++) {
            Rect rect = rects.get(i);
            drawRectangle(imageWithDrawedRects, rect);
        }
        return imageWithDrawedRects;
    }

    private void drawRectangle(Mat imageWithDrawedRects, Rect rect) {
        Core.rectangle(imageWithDrawedRects, rect.tl(), rect.br(), new Scalar(0, 0, 255, 255), 2);
    }

    private void drawBlueRectangle(Mat imageWithDrawedRects, Rect rect) {
        Core.rectangle(imageWithDrawedRects, rect.tl(), rect.br(), new Scalar(255, 0, 0, 255), 2);
    }

    public Mat localizeProbablyPlateArea(Mat image) {
        Mat edges = new Mat();
        image.copyTo(edges);
        int erosion_size_y = 12;
        int erosion_size_x = 2;
        Mat element = Imgproc.getStructuringElement(Imgproc.MORPH_RECT, new Size(2 * erosion_size_y + 1, 2 * erosion_size_x + 1));
        close(edges, element);
        erosion_size_y = 2;
        erosion_size_x = 2;
        element = Imgproc.getStructuringElement(Imgproc.MORPH_RECT, new Size(2 * erosion_size_y + 1, 2 * erosion_size_x + 1));
        open(edges, element);
        return edges;
    }

    private Mat getBinarizedImageWithEdges(Mat image) {
        Mat imageWithAreas = new Mat();
        image.copyTo(imageWithAreas);
        imageWithAreas = ImageUtils.detectVerticalEdges(imageWithAreas);
        Imgproc.threshold(imageWithAreas, imageWithAreas, THRESH, 255, Imgproc.THRESH_BINARY);
        return imageWithAreas;
    }

    private void removePlateAreasWithNoProperSize(List<Rect> rects) {
        Iterator<Rect> iterator = rects.iterator();
        while (iterator.hasNext()) {
            Rect rect = iterator.next();
            if (!hasProperSize(rect.size())) {
                iterator.remove();
            }
        }
    }

    private boolean hasProperSize(Size size) {
        double height = size.height;
        double width = size.width;
        return height < MAX_HEIGHT && height > MIN_HEIGHT && width < MAX_WIDTH && width > MIN_WIDTH;
    }

    private void removePlateAreasWithNoProperRatio(List<Rect> rects) {
        Iterator<Rect> iterator = rects.iterator();
        while (iterator.hasNext()) {
            Rect rect = iterator.next();
            double ratio = rect.height / (double) rect.width;
            if (!hasProperRatio(ratio)) {
                iterator.remove();
            }
        }
    }

    private void removePlateAreasWithNoProperEdgesNumber(List<Rect> rects, Mat imageOfEdges) {
        Iterator<Rect> iterator = rects.iterator();
        int middleEdges, thirdEdges, twoThirdEdges;
        while (iterator.hasNext()) {
            Rect rect = iterator.next();
            middleEdges = calculateVerticalEdgesInArea(imageOfEdges.submat(rect), rect.height / 2);
            thirdEdges = calculateVerticalEdgesInArea(imageOfEdges.submat(rect), rect.height / 3);
            twoThirdEdges = calculateVerticalEdgesInArea(imageOfEdges.submat(rect), 2 * rect.height / 3);
            if (!areEnoughtEdges(middleEdges, thirdEdges, twoThirdEdges)) {
                iterator.remove();
            }
        }
    }

    private boolean areEnoughtEdges(int... edgesNumber) {
        int vote = 0;
        for (int edge : edgesNumber) {
            if (edge > MIN_EDGES_NR && edge < MAX_EDGES_NR) {
                vote++;
            }
        }
        return vote > edgesNumber.length / 2;
    }

    private int calculateVerticalEdgesInArea(final Mat image, final int row) {
        double[] first = image.get(row, 0);
        int edges = 0;
        for (int col = 1; col < image.cols(); col++) {
            double[] second = image.get(row, col);
            if (first[0] != second[0]) {
                edges++;
            }
            first = second;
        }
        return edges;
    }



    private boolean hasProperRatio(double ratio) {
        return ratio > LOWEST_RATIO && ratio < HIGHEST_RATIO;
    }

    private List<Rect> getRectanglesOfPossiblePlateAreas(Mat imageWithProbablyPlateAreas) {
        List<MatOfPoint> contours = findImageContours(imageWithProbablyPlateAreas);
        ArrayList<Rect> rects = new ArrayList<>();
        for (int i = 0; i < contours.size(); i++) {
            rects.add(Imgproc.boundingRect(contours.get(i)));
        }
        return rects;
    }

    private List<MatOfPoint> findImageContours(Mat image) {
        Mat copy = new Mat();
        image.copyTo(copy);
        List<MatOfPoint> contours = new ArrayList<>();
        Imgproc.findContours(copy, contours, new Mat(), Imgproc.RETR_CCOMP,
                Imgproc.CHAIN_APPROX_NONE);
        return contours;
    }

    private void open(Mat image, Mat element) {
        Imgproc.erode(image, image, element);
        Imgproc.dilate(image, image, element);
    }

    private void close(Mat image, Mat element) {
        Imgproc.dilate(image, image, element);
        Imgproc.erode(image, image, element);
    }
}
