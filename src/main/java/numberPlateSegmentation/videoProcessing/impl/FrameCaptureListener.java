package numberPlateSegmentation.videoProcessing.impl;

import java.awt.image.BufferedImage;

/**
 * Created by wojci on 03.04.2016.
 */
public interface FrameCaptureListener {
    void doAction(BufferedImage image);
}
