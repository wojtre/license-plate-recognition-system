package numberPlateSegmentation.videoProcessing.impl;

/**
 * Created by wojci on 03.04.2016.
 */

import com.xuggle.xuggler.*;

import javax.swing.*;
import java.awt.image.BufferedImage;

/**
 * Created by Wojciech on 10-Oct-15.
 */
public class FrameCapture implements Runnable {
    private static final double SECONDS_BETWEEN_FRAMES = 0.02;
    private static final long NANO_SECONDS_BETWEEN_FRAMES =
            (long) (Global.DEFAULT_PTS_PER_SECOND * SECONDS_BETWEEN_FRAMES);
    private static long lastFrameTime = Global.NO_PTS;
    private String filename;
    private boolean run = false;
    private FrameCaptureListener frameCaptureListener;

    public void addListener(FrameCaptureListener frameCaptureListener){
        this.frameCaptureListener = frameCaptureListener;
    }
    public void terminate(){
        run = false;
    }

    public FrameCapture(String filename) {
        this.filename = filename;
    }

    @Override
    public void run() {
        run = true;
            lastFrameTime = Global.NO_PTS;
        validateIVideoResamplerIsSupported();
        IContainer container = null;
        try {
            container = getContainer(filename);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(new JFrame(), "Wybrano niewlasciwy plik!", "Wystapil blad", JOptionPane.ERROR_MESSAGE);
            return;
        }
        int videoStreamId = getFirstVideoStreamId(container);
        IStreamCoder videoCoder = getStreamCoder(container, videoStreamId);
        openVideoCoder(videoCoder);
        IVideoResampler resampler = getVideoResampler(videoCoder);
        IPacket packet = IPacket.make();
        while (container.readNextPacket(packet) >= 0 && run) {

            // Now we have a packet, let's see if it belongs to our video strea
            if (doesPacketBelongToStream(videoStreamId, packet)) {
                // We allocate a new picture to get the data out of Xuggle

                IVideoPicture picture = IVideoPicture.make(videoCoder.getPixelType(),
                        videoCoder.getWidth(), videoCoder.getHeight());
                int offset = 0;
                while (offset < packet.getSize()) {
                    int bytesDecoded = videoCoder.decodeVideo(picture, packet, offset);
                    if (bytesDecoded < 0)
                        throw new RuntimeException("got error decoding video in: " + filename);
                    offset += bytesDecoded;
                    if (picture.isComplete()) {
                        BufferedImage frame = captureFrameIfTimePassed(resampler, picture);
                        frameCaptureListener.doAction(frame);
                    }
                }
            }
        }
        closeResources(container, videoCoder);
    }

    private boolean doesPacketBelongToStream(int videoStreamId, IPacket packet) {
        return packet.getStreamIndex() == videoStreamId;
    }

    private void openVideoCoder(IStreamCoder videoCoder) {
        if (videoCoder.open(null, null) < 0)
            throw new RuntimeException(
                    "could not open video decoder for container");
    }

    private IVideoResampler getVideoResampler(IStreamCoder videoCoder) {
        IVideoResampler resampler = null;
        if (videoCoder.getPixelType() != IPixelFormat.Type.BGR24) {
            resampler = IVideoResampler.make(
                    videoCoder.getWidth(), videoCoder.getHeight(), IPixelFormat.Type.BGR24,
                    videoCoder.getWidth(), videoCoder.getHeight(), videoCoder.getPixelType());
            if (resampler == null)
                throw new RuntimeException(
                        "could not create color space resampler  ");
        }
        return resampler;
    }

    private IStreamCoder getStreamCoder(IContainer container, int videoStreamId) {
        IStream stream = container.getStream(videoStreamId);

        return stream.getStreamCoder();
    }

    private IContainer getContainer(String filename) {
        IContainer container = IContainer.make();

        if (container.open(filename, IContainer.Type.READ, null) < 0)
            throw new IllegalArgumentException("could not open file: " + filename);
        return container;
    }

    private int getFirstVideoStreamId(IContainer container) {
        int videoStreamId = -1;
        int numStreams = container.getNumStreams();
        for (int i = 0; i < numStreams; i++) {
            IStreamCoder coder = getStreamCoder(container, i);
            if (coder.getCodecType() == ICodec.Type.CODEC_TYPE_VIDEO) {
                videoStreamId = i;
                break;
            }
        }
        validateVideoStream(videoStreamId);
        return videoStreamId;
    }

    private void validateVideoStream(int videoStreamId) {
        if (videoStreamId == -1)
            throw new RuntimeException("could not find video stream in container ");
    }

    private void closeResources(IContainer container, IStreamCoder videoCoder) {
        if (videoCoder != null) {
            videoCoder.close();
        }
        if (container != null) {
            container.close();
        }
    }

    private BufferedImage captureFrameIfTimePassed(IVideoResampler resampler, IVideoPicture picture) {
        calculateTimeForFirstFrame(picture);
        if (shouldCaptureFrame(picture)) {
            IVideoPicture newPic = picture;
            lastFrameTime += NANO_SECONDS_BETWEEN_FRAMES;
            if (isVideoInBGR24Format(resampler)) {
                newPic = convertVideoToBGR24Format(resampler, picture);
            }
            return Utils.videoPictureToImage(newPic);
        }
        return null;
    }

    private void calculateTimeForFirstFrame(IVideoPicture picture) {
        if (lastFrameTime == Global.NO_PTS) {
            lastFrameTime = picture.getPts() - NANO_SECONDS_BETWEEN_FRAMES;
        }
    }

    private boolean shouldCaptureFrame(IVideoPicture picture) {
        return picture.getPts() - lastFrameTime >= NANO_SECONDS_BETWEEN_FRAMES;
    }

    private void validateIVideoResamplerIsSupported() {
        if (!IVideoResampler.isSupported(
                IVideoResampler.Feature.FEATURE_COLORSPACECONVERSION))
            throw new RuntimeException(
                    "you must install the GPL version of Xuggler (with IVideoResampler" +
                            " support) for this demo to work");
    }

    private IVideoPicture convertVideoToBGR24Format(IVideoResampler resampler, IVideoPicture picture) {
        IVideoPicture newPic;
        newPic = IVideoPicture.make(
                resampler.getOutputPixelFormat(), picture.getWidth(),
                picture.getHeight());
        if (resampler.resample(newPic, picture) < 0) {
            throw new RuntimeException(
                    "could not resample video ");
        }
        if (newPic.getPixelType() != IPixelFormat.Type.BGR24) {
            throw new RuntimeException(
                    "could not decode video as BGR 24 bit data ");
        }
        return newPic;
    }

    private boolean isVideoInBGR24Format(IVideoResampler resampler) {
        return resampler != null;
    }
}
