package numberPlateSegmentation.machineLearning.utilities;

import org.opencv.core.Mat;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.io.File;
import java.io.IOException;

/**
 * Created by Arnold on 2016-05-08.
 */
public class ImageUtilities {
    public static BufferedImage loadImage(String imageFilePath) throws IOException {
        File imageFile = new File(imageFilePath);
        BufferedImage image = null;
        if (!imageFile.exists())
            return null;

        try {
            image = ImageIO.read(imageFile);
        } catch (IOException e) {
            throw new IOException("problem with file: " + imageFilePath);
        }
        return image;
    }

    public static BufferedImage scaleImage(BufferedImage image, int newWidth, int newHeight) {
        if (image == null)
            return null;
        BufferedImage scaledImage = new BufferedImage(newWidth, newHeight, BufferedImage.TYPE_BYTE_GRAY);
        Graphics2D g2 = (Graphics2D) scaledImage.createGraphics();
        g2.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
        g2.drawImage(image, 0, 0, newWidth, newHeight, null);
        g2.dispose();
        return scaledImage;
    }

    public static BufferedImage loadScaledImage(String imageFilePath, int newWidth, int newHeight) throws IOException {
        BufferedImage loadedImage = loadImage(imageFilePath);
        return scaleImage(loadedImage, newWidth, newHeight);
    }


    public static double[] BufferedImageToDoubleArray(BufferedImage image) {
        byte[] bytes = getImageByteBuffer(image);
        double[] doubles = new double[bytes.length];
        for (int i = 0; i < bytes.length; i++) {
            if (bytes[i] < 0)
                doubles[i] = 1.0D;
            else
                doubles[i] = 0.0D;
        }
        return doubles;
    }

    public static BufferedImage Mat2BufferedImage(Mat m) {
        // source:
        // http://answers.opencv.org/question/10344/opencv-java-load-image-to-gui/
        // Fastest code
        // The output can be assigned either to a BufferedImage or to an Image

        int type = BufferedImage.TYPE_BYTE_GRAY;
        if (m.channels() > 1) {
            type = BufferedImage.TYPE_3BYTE_BGR;
        }
        int bufferSize = m.channels() * m.cols() * m.rows();
        byte[] b = new byte[bufferSize];
        m.get(0, 0, b); // get all the pixels
        BufferedImage image = new BufferedImage(m.cols(), m.rows(), type);
        final byte[] targetPixels = ((DataBufferByte) image.getRaster()
                .getDataBuffer()).getData();
        System.arraycopy(b, 0, targetPixels, 0, b.length);
        return image;

    }

    private static byte[] getImageByteBuffer(BufferedImage image) {
        return ((DataBufferByte) image.getRaster().getDataBuffer()).getData();
    }
}
