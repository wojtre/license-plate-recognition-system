package numberPlateSegmentation.machineLearning.impl;

import numberPlateSegmentation.machineLearning.interfaces.BasicAlgorithmService;
import numberPlateSegmentation.machineLearning.interfaces.ImageContainerService;
import numberPlateSegmentation.machineLearning.interfaces.ImageService;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Arnold on 2016-05-09.
 */
public class NearestMean implements BasicAlgorithmService {
    private List<BasicPatternInfo> centers;
    private int inputSize;
    private int outputSize;


    public NearestMean(ImageContainerService container) {
        this.inputSize = container.getSingleImageDataSize();
        this.outputSize = container.getImageLabels().size();
        this.centers = new ArrayList<>(this.outputSize);
        countMeans(container);
    }

    public static kNearestNeighbour load(String path) throws IOException, ClassNotFoundException {
        FileInputStream inputStream = new FileInputStream(path);
        ObjectInputStream objectInputStream = new ObjectInputStream(inputStream);
        return (kNearestNeighbour) objectInputStream.readObject();
    }

    private void countMeans(ImageContainerService container) {
        List<String> labels = container.getImageLabels();
        for (String label : labels) {
            BasicPatternInfo centerInfo = countMean(container.getImages(), label);
            centers.add(centerInfo);
        }
    }

    private BasicPatternInfo countMean(List<BasicPatternInfo> images, String Key) {
        double[] center = new double[this.inputSize];
        double counts = 0.0D;
        for (BasicPatternInfo patternInfo : images) {
            if (patternInfo.getLabel().contains(Key)) {
                center = this.addDoubleArray(center, patternInfo.getData());
                counts++;
            }
        }
        center = this.divideDoubleArrayPerScalar(center, counts);
        return new BasicPatternInfo(Key, center);
    }

    private double[] addDoubleArray(double[] d1, double[] d2) {
        if (d1.length != d2.length) {
            return null;
        }
        double[] d = new double[d1.length];
        for (int i = 0; i < d1.length; i++) {
            d[i] = d1[i] + d2[i];
        }
        return d;
    }

    private double[] divideDoubleArrayPerScalar(double[] d, double scalar) {
        double[] doubles = new double[d.length];
        for (int i = 0; i < d.length; i++) {
            doubles[i] = d[i] / scalar;
        }
        return doubles;
    }

    @Override
    public String response(ImageService pattern) throws BadAlgorithmException {
        if (pattern.getData().length != this.inputSize) {
            throw new BadAlgorithmException("Incompatible sizes of inputs");
        }
        double[] distances = this.getAllDistances(centers,pattern);
        return this.countWinner(distances);
    }

    private double distance(ImageService target, ImageService pattern) {
        double distance = 0.0D;
        double[] d1 = target.getData();
        double[] d2 = pattern.getData();
        for (int i = 0; i < d1.length; i++) {
            distance += ((d1[i] - d2[i]) * (d1[i] - d2[i]));
        }
        distance = Math.sqrt(distance);
        return distance;
    }

    private double[] getAllDistances(List<BasicPatternInfo> neighbours, ImageService target) {
        double[] allDistances = new double[neighbours.size()];
        int i = 0;
        for (BasicPatternInfo pattern : neighbours) {
            allDistances[i] = this.distance(target, pattern);
            i++;
        }
        return  allDistances;
    }

    private String countWinner(double[] distances){
        double distance = Double.MAX_VALUE;
        int winner = 0;
        for (int i = 0; i < distances.length; i++) {
            if (distances[i] < distance){
                winner = i;
                distance = distances[i];
            }
        }
        return centers.get(winner).getLabel();
    }

    @Override
    public int getInputSize() {
        return this.inputSize;
    }

    @Override
    public int getOutputSize() {
        return this.outputSize;
    }

    @Override
    public void save(String path) throws IOException {
        FileOutputStream outputStream = new FileOutputStream(path);
        ObjectOutputStream objectInputStream = new ObjectOutputStream(outputStream);
        objectInputStream.writeObject(this);
    }

}
