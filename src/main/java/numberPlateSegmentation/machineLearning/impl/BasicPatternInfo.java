package numberPlateSegmentation.machineLearning.impl;

import numberPlateSegmentation.machineLearning.interfaces.ImageService;
import numberPlateSegmentation.machineLearning.utilities.ImageUtilities;

import java.awt.image.BufferedImage;
import java.io.Serializable;

/**
 * Created by Arnold on 2016-05-08.
 */
public class BasicPatternInfo implements ImageService,Serializable {
    private String label;
    private double[] data;

    public BasicPatternInfo() {
        label = null;
        data = null;
    }

    public BasicPatternInfo(String label, double[] data) {
        this.label = label.toUpperCase();
        this.data = data;
    }
    public BasicPatternInfo(BufferedImage bufferedImage, int width, int heigth){
        BufferedImage scaledImage = ImageUtilities.scaleImage(bufferedImage, width, heigth);
        this.data = ImageUtilities.BufferedImageToDoubleArray(scaledImage);
    }

    @Override
    public String getLabel() {
        return this.label;
    }

    @Override
    public void setLabel(String label) {
        this.label = label.toUpperCase();
    }

    @Override
    public double[] getData() {
        return data;
    }

    @Override
    public void setData(double[] data) {
        this.data = data;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append(this.label);
        for (double d : this.data) {
            builder.append(" ");
            builder.append(d);
        }
        return builder.toString();
    }
}
