package numberPlateSegmentation.machineLearning.impl;

import Jama.Matrix;
import numberPlateSegmentation.machineLearning.enums.AlgorithmType;
import numberPlateSegmentation.machineLearning.interfaces.BasicAlgorithmService;
import numberPlateSegmentation.machineLearning.interfaces.ImageContainerService;
import numberPlateSegmentation.machineLearning.interfaces.ImageService;
import org.encog.ml.svm.KernelType;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Arnold on 2016-05-08.
 */
public class AlgorithmValidation {


    public static double validate(ImageContainerService container, BasicAlgorithmService basicAlgorithm) {
        double imageCount = 0D;
        double goodAnswers = 0D;
        for (ImageService image : container.getImages()) {
            String imageLabel = image.getLabel();
            String answerLabel = null;
            try {
                answerLabel = basicAlgorithm.response(image);
            } catch (BadAlgorithmException e) {
                e.printStackTrace();
            }
            imageCount++;
            if (imageLabel.equals(answerLabel)) {
                goodAnswers++;
            }
        }

        double result;
        if (imageCount > 0D) {
            result = (goodAnswers / imageCount);
        } else {
            result = 1D;
        }
        return result;
    }

    public static String crossValidation(ImageContainerService container, AlgorithmType type, int kSets, int... neurons) {
        double[] results = new double[kSets];
        StringBuilder report = new StringBuilder();
        addLineToReport(report, "Algorithm ", type);

        for (int i = 1; i <= kSets; i++) {
            //utworzenie zbioru uczacego i testujacego
            ImageSetPair set = new ImageSetPair(container, i, kSets);
            //utworzenie algorytmu
            BasicAlgorithmService algorithm = createAlgorithm(set.getTrainingSet(), type, neurons);
            //Walidacja algorytmu
            results[i - 1] = AlgorithmValidation.validate(set.getValidationSet(), algorithm);
            BigDecimal bigDecimal = new BigDecimal(String.valueOf(100 * results[i - 1])).setScale(2, BigDecimal.ROUND_HALF_UP);
            addLineToReport(report, "iteration: ", i, ", result = ", bigDecimal, "%");
        }
        //count mean of good recognition
        double sum = sumDoubleArray(results);
        BigDecimal bigDecimal = new BigDecimal(String.valueOf(100 * sum / kSets)).setScale(2, BigDecimal.ROUND_HALF_UP);
        addLineToReport(report, kSets, "-crossvalidation give " + bigDecimal + "% of recognized signs");
        //count standardDeviation
        double standardDeviation = countStandardDeviation(results);
        bigDecimal = new BigDecimal(String.valueOf(100.0 * standardDeviation)).setScale(2, BigDecimal.ROUND_HALF_UP);
        addLineToReport(report, "Standard Deviation = ", bigDecimal + "%\n");
        return report.toString();
    }


    public static String confusionMatrixResult(ImageContainerService container, AlgorithmType type, int kSets, int...neurons) {
        double[] results = new double[kSets];
        StringBuilder report = new StringBuilder();
        List<String> labels = container.getImageLabels();
        final int matrixSize = labels.size();
        Matrix confusionMatrix = new Matrix(matrixSize, matrixSize);
        //addLineToReport(report,"Algorithm ",type);
        for (int i = 1; i <= kSets; i++) {
            //utworzenie zbioru uczacego i testujacego
            ImageSetPair set = new ImageSetPair(container, i, kSets);
            //utworzenie klasyfikatora
            BasicAlgorithmService classifier = createAlgorithm(set.getTrainingSet(), type, neurons);
            Matrix confusionMatrixPart = validateIntoConfusionMatrix(set.getValidationSet(), labels, classifier);
            confusionMatrix = confusionMatrix.plus(confusionMatrixPart);
        }
        for (String label : labels) {
            double signAccuracy = countSignAccuracy(confusionMatrix, labels, label);
            BigDecimal bigDecimal = new BigDecimal(String.valueOf(100.0 * signAccuracy)).setScale(2, BigDecimal.ROUND_HALF_UP);
            String wrongSign = returnMostCommonFalseAnswerSign(confusionMatrix, labels, label);
            double wrongSignRatio = countFalseAnswerRatioForSign(label,wrongSign,confusionMatrix,labels);
            BigDecimal bigDecimal2 = new BigDecimal(String.valueOf(100.0 * wrongSignRatio)).setScale(2, BigDecimal.ROUND_HALF_UP);
            addLineToReport(report, label, ": accuracy = ", bigDecimal, "%", "  Most Common wrong answer = " + wrongSign + " Ratio = " + bigDecimal2 + "%");

        }

        return report.toString();


    }

    private static double countSignAccuracy(Matrix confusionMatrix, List<String> labels, String sign) {
        final int row = labels.indexOf(sign);
        double signQuantity = 0.0D;
        for (int j = 0; j < confusionMatrix.getColumnDimension(); j++) {
            signQuantity += confusionMatrix.get(row, j);
        }
        if (signQuantity != 0) {
            return confusionMatrix.get(row, row) / signQuantity;
        } else {
            System.out.println("Bad quantity for " + sign + " =" + signQuantity);
            return 1.0D;
        }
    }

    private static String returnMostCommonFalseAnswerSign(Matrix confusionMatrix, List<String> labels, String sign) {
        final int row = labels.indexOf(sign);
        double MaxValue = 0.0D;
        int MaxValuePosition = -1;

        for (int j = 0; j < confusionMatrix.getColumnDimension(); j++) {
            if (row != j) {
                if (confusionMatrix.get(row, j) > MaxValue) {
                    MaxValue = confusionMatrix.get(row, j);
                    MaxValuePosition = j;
                }
            }
        }
        if (MaxValuePosition >= 0) {
            return labels.get(MaxValuePosition);
        }
        return "no mistakes";
    }

    private static double countFalseAnswerRatioForSign(String correctSign, String falseSign, Matrix confusionMatrix, List<String> labels) {
        final int row = labels.indexOf(correctSign);
        final int col = labels.indexOf(falseSign);
        if (col < 0 || row < 0){
            return 0.0D;
        }
        double signQuantity = 0.0D;
        for (int j = 0; j < confusionMatrix.getColumnDimension(); j++) {
            signQuantity += confusionMatrix.get(row, j);
        }
        double falseSignQuantity = confusionMatrix.get(row, col);
        if (Double.compare(signQuantity, 0.0D) != 0) {
            return falseSignQuantity / signQuantity;
        }
        return 0.0D;
    }


    private static Matrix validateIntoConfusionMatrix(ImageContainerService testContainer, List<String> labels, BasicAlgorithmService classifier) {
        String answer = null;
        final int size = labels.size();
        Matrix newMatrix = new Matrix(size, size);

        for (ImageService image : testContainer.getImages()) {
            try {
                answer = classifier.response(image);

            } catch (BadAlgorithmException e) {
                e.printStackTrace();
            }
            final int posX = labels.indexOf(image.getLabel());
            final int posY = labels.indexOf(answer);
            if (posX >= 0 || posY >= 0) {
                double v = newMatrix.get(posX, posY);
                newMatrix.set(posX, posY, v + 1.0D);
            }
        }
        return newMatrix;
    }


    private static double sumDoubleArray(double[] doubles) {
        double sum = 0.0D;
        for (Double aDouble : doubles) {
            sum += aDouble;
        }
        return sum;
    }

    private static void addLineToReport(StringBuilder builder, Object... objects) {
        if (builder == null) {
            return;
        }
        for (Object object : objects) {
            builder.append(object);
        }
        builder.append('\n');
    }

    private static double countStandardDeviation(double[] data) {
        double mean = 0.0D;
        for (double d : data) {
            mean += d;
        }
        mean /= data.length;
        double sumDifferencesDataAndMean = 0.0D;
        for (double v : data) {
            double difference = v - mean;
            sumDifferencesDataAndMean += Math.pow(difference, 2);
        }
        //return deviation
        return Math.sqrt(sumDifferencesDataAndMean / data.length);
    }

    private static BasicAlgorithmService createAlgorithm(ImageContainerService container, AlgorithmType type, int... neurons) {
        BasicAlgorithmService algorithmService = null;
        if (type.equals(AlgorithmType.SUPPORT_VECTOR_MACHINE)) {
            algorithmService = new SvmClassifier(container, KernelType.Linear, 1);
        }
        if (type.equals(AlgorithmType.K_NEAREST_NEIGHBOUR)) {
            algorithmService = new kNearestNeighbour(container);
        }
        if (type.equals(AlgorithmType.NEAREST_MEAN)) {
            algorithmService = new NearestMean(container);
        }
        if (type.equals(AlgorithmType.FFNEURALNETWORK)){
            algorithmService = new FFNeuralNetwork(container,neurons);
        }
        return algorithmService;
    }
}

class ImageSetPair {
    private ImageContainerService trainingSet;
    private ImageContainerService validationSet;


    public ImageSetPair(ImageContainerService mainSet, int iteration, int kSets) {
        if (mainSet == null)
            return;
        this.trainingSet = new ImageInfoContainer(mainSet);
        this.validationSet = new ImageInfoContainer(mainSet);

        ArrayList<String> labels = mainSet.getImageLabels();
        for (String key : labels) {
            List<BasicPatternInfo> imageList = mainSet.getImages(key);
            final int valElemQuantity = imageList.size() / kSets;
            final int min = (iteration - 1) * valElemQuantity;
            final int max = iteration * valElemQuantity;
            for (int i = 0; i < imageList.size(); i++) {
                if (i >= min && i < max) {
                    this.validationSet.addImage(imageList.get(i));
                } else {
                    this.trainingSet.addImage(imageList.get(i));
                }
            }
        }
    }

    public ImageContainerService getTrainingSet() {
        return trainingSet;
    }

    public ImageContainerService getValidationSet() {
        return validationSet;
    }
}
