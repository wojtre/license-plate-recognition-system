package numberPlateSegmentation.machineLearning.impl;

import numberPlateSegmentation.machineLearning.interfaces.BasicAlgorithmService;
import numberPlateSegmentation.machineLearning.interfaces.ImageContainerService;
import numberPlateSegmentation.machineLearning.interfaces.ImageService;
import org.encog.engine.network.activation.ActivationSigmoid;
import org.encog.ml.data.MLData;
import org.encog.ml.data.MLDataSet;
import org.encog.ml.data.basic.BasicMLData;
import org.encog.ml.data.basic.BasicMLDataSet;
import org.encog.ml.train.MLTrain;
import org.encog.ml.train.strategy.StopTrainingStrategy;
import org.encog.neural.networks.BasicNetwork;
import org.encog.neural.networks.layers.BasicLayer;
import org.encog.neural.networks.training.propagation.resilient.ResilientPropagation;

import java.io.*;
import java.util.ArrayList;

/**
 * Created by Arnold on 2016-06-04.
 */
public class FFNeuralNetwork implements BasicAlgorithmService {
    private BasicNetwork network;
    private int inputSize;
    private ArrayList<String> outputMap;
    private ArrayList<Integer> neuronsQuantityInHiddenLayers;
    transient private ImageContainerService container;

    public FFNeuralNetwork(ImageContainerService container, int... hiddenLayersNeuronsQuantity) {
        if (container == null) {
            return;
        }
        this.container = container;
        outputMap = container.getImageLabels();
        inputSize = container.getSingleImageDataSize();
        neuronsQuantityInHiddenLayers = new ArrayList<>();
        for (int i : hiddenLayersNeuronsQuantity) {
            neuronsQuantityInHiddenLayers.add(i);
        }
        this.createNetwork();
        this.trainNetwork();
    }


    @Override
    public String response(ImageService pattern) throws BadAlgorithmException {
        if (pattern.getData().length != this.inputSize)
            throw new BadAlgorithmException("Size of input data is not equal to classifier inputSize");
        final MLData answer = this.network.compute(new BasicMLData(pattern.getData()));
        //znajdz najwieksza wartosc
        double[] doubleResults = answer.getData();
        int index = 0;
        //szukaj wartosci najblizszej 1
        double distance = Double.MAX_VALUE;
        for (int k = 0; k < doubleResults.length; k++) {
            double tempDistance = Math.abs(1.0D - doubleResults[k]);
            if (distance > tempDistance) {
                distance = tempDistance;
                index = k;
            }
        }
        return this.outputMap.get(index);
    }


    public static FFNeuralNetwork load(String path) throws IOException, ClassNotFoundException {
        FileInputStream inputStream = new FileInputStream(path);
        ObjectInputStream objectInputStream = new ObjectInputStream(inputStream);
        FFNeuralNetwork neuralNetwork = null;
        neuralNetwork = (FFNeuralNetwork) objectInputStream.readObject();
        return neuralNetwork;
    }

    @Override
    public void save(String path) throws IOException {
        FileOutputStream output = new FileOutputStream(path);
        ObjectOutputStream objectOutputStream = new ObjectOutputStream(output);
        objectOutputStream.writeObject(this);
    }

    @Override
    public int getInputSize() {
        return inputSize;
    }

    @Override
    public int getOutputSize() {
        return outputMap.size();
    }


    private void createNetwork() {
        network = new BasicNetwork();
        network.addLayer(new BasicLayer(null, true, this.inputSize));
        for (Integer neuronsQuantity : neuronsQuantityInHiddenLayers) {
            network.addLayer(new BasicLayer(new ActivationSigmoid(), true, neuronsQuantity));
        }
        network.addLayer(new BasicLayer(new ActivationSigmoid(), false, this.outputMap.size()));
        network.getStructure().finalizeStructure();

    }

    private void trainNetwork() {
        network.reset();
        if (container != null) {
            MLDataSet trainingSet = createDataSet();
            //MLTrain training = new LevenbergMarquardtTraining(network, trainingSet);
            MLTrain training = new ResilientPropagation(network, trainingSet);
            /**
             *  możesz zmniejszyc pierwszy parametr tylko siec bedzie dluzej sie uczyc
             *  możesz też bawić sie drugim parametrem który oznacza po ilu epokach uczenia,
             *  jesli blad nie zmieni sie o wiecej niz wartość pierwszego parametru, nalezy zakonczyc
             *  procedure uczenia
             */
            StopTrainingStrategy stop = new StopTrainingStrategy(0.01, 100);
            training.addStrategy(stop);

            int epoch = 0;
            while (!stop.shouldStop()) {
                training.iteration();
                epoch++;
                if ((epoch % 100) == 0)
                    System.out.println("########Epoch: " + epoch + " ,Error rate = " + training.getError());
            }
        }
    }

    private MLDataSet createDataSet() {
        MLDataSet dataSet = new BasicMLDataSet();
        for (ImageService pattern : this.container.getImages()) {
            MLData input = new BasicMLData(pattern.getData());
            MLData output = new BasicMLData(StringToDoubleOutput(pattern.getLabel()));
            dataSet.add(input, output);
        }
        return dataSet;
    }


    private double[] StringToDoubleOutput(String key) {
        double[] output = new double[outputMap.size()];
        for (int k = 0; k < output.length; k++) {
            output[k] = 0.0;
        }
        int index = outputMap.indexOf(key);
        //System.out.println(index + " " + key);
        output[index] = 1.00;
        return output;
    }


}
