package numberPlateSegmentation.machineLearning.impl;

import numberPlateSegmentation.machineLearning.interfaces.BasicAlgorithmService;
import numberPlateSegmentation.machineLearning.interfaces.ImageContainerService;
import numberPlateSegmentation.machineLearning.interfaces.ImageService;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Arnold on 2016-05-09.
 */
public class kNearestNeighbour implements BasicAlgorithmService {
    public final static int kAttr = 5;
    private List<BasicPatternInfo> neighbours;

    private int inputSize;
    private ArrayList<String> outputMap;


    public kNearestNeighbour(ImageContainerService container) {
        this.neighbours = container.getImages();
        this.outputMap = container.getImageLabels();
        if (this.neighbours != null && this.neighbours.size() > 0) {
            this.inputSize = neighbours.get(0).getData().length;
        }
    }

    public static kNearestNeighbour load(String path) throws IOException, ClassNotFoundException {
        FileInputStream fileInputStream = new FileInputStream(path);
        ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
        return (kNearestNeighbour) objectInputStream.readObject();
    }

    @Override
    public String response(ImageService pattern) throws BadAlgorithmException {
        if (pattern.getData().length != this.inputSize) {
            throw new BadAlgorithmException("incompatible input sizes");
        }
        double[] allDistances = this.getAllDistances(neighbours,pattern);
        int[] winIndexes = this.winnersIndexes(allDistances);
        String[] winLabels = this.transformIndexesToStrings(winIndexes);
        return this.getWinnerClass(winLabels);
    }

    private double distance(ImageService target, ImageService pattern) {
        double distance = 0.0D;
        double[] d1 = target.getData();
        double[] d2 = pattern.getData();
        for (int i = 0; i < d1.length; i++) {
            distance += ((d1[i] - d2[i]) * (d1[i] - d2[i]));
        }
        distance = Math.sqrt(distance);
        return distance;
    }

    private double[] getAllDistances(List<BasicPatternInfo> neighbours, ImageService target) {
        double[] allDistances = new double[neighbours.size()];
        int i = 0;
        for (BasicPatternInfo pattern : neighbours) {
            allDistances[i] = this.distance(target, pattern);
            i++;
        }
        return  allDistances;
    }

    private int[] winnersIndexes(double[] distances) {
        int[] winIndexes = new int[kAttr];
        boolean[] occured = new boolean[distances.length];
        for (int k = 0; k < distances.length; k++)
            occured[k] = false;
        for (int i = 0; i < kAttr; i++) {
            double min = Double.MAX_VALUE;
            int winnerIndex = 0;
            for (int index = 0; index < distances.length; index++) {
                if (!occured[index] && (min > distances[index])) {
                    min = distances[index];
                    winnerIndex = index;
                }
            }
            winIndexes[i] = winnerIndex;
            occured[winnerIndex] = true;
        }
        return winIndexes;
    }

    private String[] transformIndexesToStrings(int[] indexes) {
        String[] results = new String[indexes.length];
        for (int i = 0; i < indexes.length; i++) {
            results[i] = neighbours.get(indexes[i]).getLabel();
        }
        return results;
    }

    private String getWinnerClass(String[] strings) {
        int[] occurences = new int[strings.length];
        for (int i = 0; i < strings.length; i++)
            occurences[i] = 0;

        for (int k = 0; k < strings.length; k++) {
            String obj = strings[k];
            for (int m = 0; m < strings.length; m++) {
                if (obj.equals(strings[m]))
                    occurences[k]++;
            }
        }
        int max = -1;
        int index = 0;
        for (int i = 0; i < occurences.length; i++) {
            if (occurences[i] > max) {
                max = occurences[i];
                index = i;
            }
        }
        return strings[index];
    }

    @Override
    public int getInputSize() {
        return this.inputSize;
    }

    @Override
    public int getOutputSize() {
        return this.outputMap.size();
    }

    @Override
    public void save(String path) throws IOException {
        FileOutputStream outputStream = new FileOutputStream(path);
        ObjectOutputStream objectOutputStream = new ObjectOutputStream(outputStream);
        objectOutputStream.writeObject(this);
    }
}
