package numberPlateSegmentation.machineLearning.impl;

import numberPlateSegmentation.machineLearning.interfaces.BasicAlgorithmService;
import numberPlateSegmentation.machineLearning.interfaces.ImageContainerService;
import numberPlateSegmentation.machineLearning.interfaces.ImageService;
import org.encog.ml.data.MLData;
import org.encog.ml.data.MLDataSet;
import org.encog.ml.data.basic.BasicMLData;
import org.encog.ml.data.basic.BasicMLDataSet;
import org.encog.ml.svm.KernelType;
import org.encog.ml.svm.SVM;
import org.encog.ml.svm.SVMType;
import org.encog.ml.svm.training.SVMSearchTrain;
import org.encog.ml.train.MLTrain;

import java.io.*;
import java.util.ArrayList;

/**
 * Created by Arnold on 2016-05-08.
 */
public class SvmClassifier implements BasicAlgorithmService {
    private SVM svm;
    private int inputSize;
    private ArrayList<String> outputMap;

    public SvmClassifier(ImageContainerService container, KernelType kernelType, int maxIter) {
        if (container == null)
            return;
        this.outputMap = container.getImageLabels();
        this.inputSize = container.getSingleImageDataSize();
        this.trainSvmClassifier(container, KernelType.Linear, maxIter);

    }

    public static SvmClassifier load(String path) throws IOException, ClassNotFoundException {
        SvmClassifier svmClassifier = null;
        FileInputStream inputStream = new FileInputStream(path);
        ObjectInputStream objectInputStream = new ObjectInputStream(inputStream);
        svmClassifier = (SvmClassifier) objectInputStream.readObject();
        return svmClassifier;
    }

    //TODO
    @Override
    public String response(ImageService pattern) throws BadAlgorithmException {
        if (pattern.getData().length != this.inputSize)
            throw new BadAlgorithmException("Size of inputdata is not equal to classifier inputSize");
        MLData answer = this.svm.compute(new BasicMLData(pattern.getData()));
        int position = (int)((answer.getData())[0]);
        return this.outputMap.get(position);
    }

    @Override
    public int getInputSize() {
        return this.inputSize;
    }

    @Override
    public int getOutputSize() {
        return this.outputMap.size();
    }

    @Override
    public void save(String path) throws IOException {
        FileOutputStream outputStream = new FileOutputStream(path);
        ObjectOutputStream objectStream = new ObjectOutputStream(outputStream);
        objectStream.writeObject(this);
    }

    private void trainSvmClassifier(ImageContainerService container, KernelType kernelType, int maxIter) {
        this.svm = new SVM(this.inputSize, SVMType.SupportVectorClassification, kernelType);
        MLDataSet dataSet = createDataSet(container);
        MLTrain train = new SVMSearchTrain(this.svm, dataSet);
        int epoch = 0;
        do {
            train.iteration();
            epoch++;
        } while ((!train.isTrainingDone()) && epoch < maxIter);
    }

    private MLDataSet createDataSet(ImageContainerService container) {
        MLDataSet dataSet = new BasicMLDataSet();
        for (ImageService pattern : container.getImages()) {
            MLData input = new BasicMLData(pattern.getData());
            MLData output = new BasicMLData(StringToDoubleArray(pattern.getLabel()));
            dataSet.add(input, output);
        }
        return dataSet;
    }

    private double[] StringToDoubleArray(String key) {
        int position = this.outputMap.indexOf(key);
        return (new double[]{(double) position});
    }
}
