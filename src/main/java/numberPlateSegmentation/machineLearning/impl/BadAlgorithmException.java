package numberPlateSegmentation.machineLearning.impl;

/**
 * Created by Arnold on 2016-05-08.
 */
public class BadAlgorithmException extends Exception {
    public BadAlgorithmException() {
    }

    public BadAlgorithmException(String message) {
        super(message);
    }
}
