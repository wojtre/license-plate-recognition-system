package numberPlateSegmentation.machineLearning.impl;

import numberPlateSegmentation.machineLearning.interfaces.BasicAlgorithmService;
import numberPlateSegmentation.machineLearning.utilities.ImageUtilities;
import org.opencv.core.Mat;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Arnold on 2016-03-13.
 */
public class LicensePlateRecognition {
    public static List<String> recognizePlate(List<List<Mat>> listOfSeperatedSignsFromPlates, BasicAlgorithmService algorithm, int width, int height) {
        List<String> listOfPlates = new ArrayList<>();
        if (listOfSeperatedSignsFromPlates == null) {
            return null;
        }
        for (List<Mat> plate : listOfSeperatedSignsFromPlates) {
            String signsFromSinglePlate = "";
            if (plate != null) {
                for (Mat sign : plate) {
                    try {
                        BasicPatternInfo image = new BasicPatternInfo(ImageUtilities.Mat2BufferedImage(sign), width, height);
                        String response = algorithm.response(image);
                        if (response.contains("OTHER")) {
                            response = " # ";
                        }
                        signsFromSinglePlate += response;
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                listOfPlates.add(signsFromSinglePlate);
            }
        }
        return listOfPlates;
    }
}