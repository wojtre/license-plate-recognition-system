package numberPlateSegmentation.machineLearning.impl;

import numberPlateSegmentation.machineLearning.interfaces.ImageContainerService;
import numberPlateSegmentation.machineLearning.interfaces.ImageService;
import numberPlateSegmentation.machineLearning.enums.DataType;
import numberPlateSegmentation.machineLearning.utilities.ImageUtilities;
import org.w3c.dom.*;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.*;

/**
 * Created by Arnold on 2016-05-08.
 */
public class ImageInfoContainer implements ImageContainerService {
    private static int WIDTH = 20;
    private static int HEIGHT = 20;
    private static int ATTR_NUM = 17;
    private List<BasicPatternInfo> imageList;
    private Histogram histogram;
    private DataType dataType;


    private ArrayList<String> imageLabels;
    private int patternDataSize;


    public ImageInfoContainer() {
        imageList = null;
        histogram = null;
    }

    public ImageInfoContainer(ImageContainerService container) {
        this.imageLabels = container.getImageLabels();
        this.patternDataSize = container.getSingleImageDataSize();
        imageList = new ArrayList<>();
        updateHistogram();
    }

    public ImageInfoContainer(String filePath) throws IOException {
        this.imageList = new ArrayList<>();

        if (filePath.contains(".csv"))
            readCSVFile(filePath, ATTR_NUM);
        if (filePath.contains(".xml"))
            readXMLFile(filePath);

        this.countImageLabels();
        this.countDataSize();
        this.imageList = this.sortImagesByLabels();


        updateHistogram();
    }

    @Override
    public void addImage(BasicPatternInfo patternInfo) {
        this.imageList.add(patternInfo);
    }

    private void readXMLFile(String filePath) throws IOException {
        Document document = loadXMLDocument(filePath);
        if (document == null)
            return;
        Element root = document.getDocumentElement();
        NodeList nodeList = root.getElementsByTagName("image");

        imageList = new ArrayList<>(nodeList.getLength());
        for (int i = 0; i < nodeList.getLength(); i++) {
            BasicPatternInfo patternInfo = this.getPatternInfoFromDocument(nodeList.item(i));
            imageList.add(patternInfo);
        }
        this.dataType = DataType.IMAGE_IS_PIXEL_SET;
    }

    private Document loadXMLDocument(String filePath) throws IOException {
        DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();
        Document document = null;
        File xmlFile = new File(filePath);
        if (!xmlFile.exists())
            return null;
        try {
            DocumentBuilder builder = builderFactory.newDocumentBuilder();
            document = builder.parse(xmlFile);
        } catch (IOException | ParserConfigurationException | SAXException ex) {
            throw new IOException(ex);
        }
        return document;
    }

    private BasicPatternInfo getPatternInfoFromDocument(Node node) {
        NodeList imageChildren = node.getChildNodes();
        String path = ((Text) imageChildren.item(1).getFirstChild()).getData().trim();
        String id = ((Text) imageChildren.item(5).getFirstChild()).getData().trim();

        BasicPatternInfo patternInfo = null;

        try {
            BufferedImage image = ImageUtilities.loadScaledImage(path, WIDTH, HEIGHT);
            double[] doubles = ImageUtilities.BufferedImageToDoubleArray(image);
            patternInfo = new BasicPatternInfo(id, doubles);
        } catch (IOException e) {
            /*!!!!!! przerobiŠ na dziennik !!!!! */
            System.err.println("Problem with loading file from : " + path);
        }

        return patternInfo;
    }

    private void readCSVFile(String filePath, int attrNum) throws IOException {
        File file = new File(filePath);

        try (Scanner scanner = new Scanner(file)) {
            while (scanner.hasNextLine()) {
                String line = scanner.nextLine();
                String[] splitLine = line.split(",");
                if (splitLine.length != attrNum)
                    throw new IOException("File has errors");
                BasicPatternInfo imgInfo = new BasicPatternInfo();
                double[] data = new double[attrNum - 1];
                if (splitLine.length > 0)
                    imgInfo.setLabel(splitLine[0]);
                for (int i = 1; i < splitLine.length; i++) {
                    data[i - 1] = Double.parseDouble(splitLine[i]);
                }
                imgInfo.setData(data);
                this.imageList.add(imgInfo);
            }
            this.dataType = DataType.IMAGE_AS_COUNTED_ATTRIBUTES;
        }
    }

    private void countDataSize() {
        int size;
        if (this.imageList == null || imageList.size() == 0) {
            size = 0;
        } else {
            size = this.imageList.get(0).getData().length;
        }
        this.patternDataSize = size;
    }

    private void countImageLabels() {
        if (this.imageList == null) {
            return;
        }
        ArrayList<String> labels = new ArrayList<>();
        for (BasicPatternInfo info : this.imageList) {
            String label = info.getLabel();
            if (!labels.contains(label)) {
                labels.add(label);
            }
        }

        labels.sort(new Comparator<String>() {
            @Override
            public int compare(String o1, String o2) {
                if (o1 != null && o2 != null) {
                    if (o1.length() != o2.length()) {
                        return o1.length() - o2.length();
                    }
                    return o1.compareTo(o2);
                }
                return 0;
            }
        });
        this.imageLabels = labels;
    }

    private ArrayList<BasicPatternInfo> sortImagesByLabels() {
        if (this.imageList == null)
            return null;
        if (imageLabels == null)
            this.countImageLabels();
        if (imageLabels == null)
            return null;

        ArrayList<BasicPatternInfo> sortedImages = new ArrayList<>(this.imageList.size());
        for (String s : this.imageLabels) {
            for (BasicPatternInfo patternInfo : this.imageList) {
                if (patternInfo.getLabel().equals(s))
                    sortedImages.add(patternInfo);
            }
        }
        return sortedImages;
    }

    @Override
    public int getSingleImageDataSize() {
        return this.patternDataSize;
    }

    @Override
    public List<BasicPatternInfo> getImages() {
        return this.imageList;
    }

    @Override
    public String getDataDescription() {
        String description = null;
        if (this.dataType.equals(DataType.IMAGE_AS_COUNTED_ATTRIBUTES)) {
            description = "contains Pattern label and " + ATTR_NUM + " attributes";
        }
        if (this.dataType.equals(DataType.IMAGE_IS_PIXEL_SET)) {
            description = "contains pattern label and data as pixel set. Width = " + WIDTH + ", Height = " + HEIGHT;
        }
        return description;
    }

    @Override
    public List<BasicPatternInfo> getImages(String key) {
        List<BasicPatternInfo> list = new ArrayList<>();
        String key2 = key.toUpperCase();
        for (BasicPatternInfo patternInfo : this.imageList) {
            if (key2.equals(patternInfo.getLabel())) {
                list.add(patternInfo);
            }
        }
        return list;
    }

    @Override
    public ArrayList<String> getImageLabels() {
        return imageLabels;
    }

    // TO DO
    @Override
    public ArrayList<String> getHistogramLabels() {
        return histogram.getLabels();
    }

    @Override
    public double[] getHistogramCounts() {
        return histogram.getQuantities();
    }

    // TO DO
    private void updateHistogram() {
        if (histogram == null)
            histogram = new Histogram(this);
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        for (ImageService imgInfo : this.imageList) {
            builder.append(imgInfo.toString());
            builder.append("\n");
        }
        return builder.toString();
    }
}

class Histogram {
    private ArrayList<String> labels;
    private double[] quantities;

    public Histogram(ImageInfoContainer container) {
        labels = container.getImageLabels();
        quantities = new double[labels.size()];
        for (int i = 0; i < labels.size(); i++) {
            quantities[i] = this.countQuantity(container, labels.get(i));
        }
    }

    public ArrayList<String> getLabels() {
        return labels;
    }

    public double[] getQuantities() {
        return quantities;
    }

    private double countQuantity(ImageInfoContainer container, String key) {
        double quantity = 0.0D;
        for (BasicPatternInfo patternInfo : container.getImages()) {
            if (patternInfo.getLabel().equals(key))
                quantity++;
        }
        return quantity;
    }
}
