package numberPlateSegmentation.machineLearning;


import numberPlateSegmentation.machineLearning.enums.AlgorithmType;
import numberPlateSegmentation.machineLearning.impl.AlgorithmValidation;
import numberPlateSegmentation.machineLearning.impl.ImageInfoContainer;
import numberPlateSegmentation.machineLearning.interfaces.ImageContainerService;

/**
 * Created by Arnold on 2016-05-08.
 */
public class Main {
    public static void main(String[] args) {
        try {
            String csvDataPath = "C:\\IdeaProjects\\ZMiTAD_projekt\\src\\main\\resources\\dataSets\\letter-recognition.csv";
            String xmlDataPath = "C:\\IdeaProjects\\EngineeringProject\\AlgorythmImplementations\\data\\xmlDataInfo.xml";
            ImageContainerService container = new ImageInfoContainer(csvDataPath);
            System.out.println("size=" + container.getImages().size());
            //System.out.println(container.getDataDescription());
            //System.out.println(container.getImageLabels());
            //System.out.println(container.getSingleImageDataSize());
            //SvmClassifier svm = new SvmClassifier(container, KernelType.Linear,1);
            String report = AlgorithmValidation.crossValidation(container, AlgorithmType.SUPPORT_VECTOR_MACHINE, 5);
            System.out.println(report);
            //svm.save("smiec");
            //SvmClassifier svm2 = SvmClassifier.load("smiec");
            //AlgorithmValidation.validate(container,svm);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
