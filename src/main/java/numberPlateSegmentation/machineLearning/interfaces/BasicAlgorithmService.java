package numberPlateSegmentation.machineLearning.interfaces;

import numberPlateSegmentation.machineLearning.impl.BadAlgorithmException;

import java.io.IOException;
import java.io.Serializable;

/**
 * Created by Arnold on 2016-05-08.
 */
public interface BasicAlgorithmService extends Serializable {
    String response(ImageService pattern) throws BadAlgorithmException;
    int getInputSize();
    int getOutputSize();

    void save(String path) throws IOException;

}
