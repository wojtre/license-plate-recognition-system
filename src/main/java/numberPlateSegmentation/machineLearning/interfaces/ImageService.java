package numberPlateSegmentation.machineLearning.interfaces;

/**
 * Created by Arnold on 2016-05-08.
 */
public interface ImageService {
    String getLabel();
    double[] getData();
    void setLabel(String label);
    void setData(double[] data);
}
