package numberPlateSegmentation.machineLearning.interfaces;

import numberPlateSegmentation.machineLearning.impl.BasicPatternInfo;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Arnold on 2016-05-08.
 */
public interface ImageContainerService {
    List<BasicPatternInfo> getImages();
    List<BasicPatternInfo> getImages(String k);
    List<String> getHistogramLabels();
    double[] getHistogramCounts();
    String getDataDescription();
    ArrayList<String> getImageLabels();
    int getSingleImageDataSize();
    void addImage(BasicPatternInfo patternInfo);
}
