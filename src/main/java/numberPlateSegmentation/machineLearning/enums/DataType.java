package numberPlateSegmentation.machineLearning.enums;

/**
 * Created by Arnold on 2016-05-08.
 */
public enum DataType {
    IMAGE_IS_PIXEL_SET, IMAGE_AS_COUNTED_ATTRIBUTES;
}
