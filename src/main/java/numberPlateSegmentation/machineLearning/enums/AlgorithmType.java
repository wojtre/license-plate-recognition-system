package numberPlateSegmentation.machineLearning.enums;

/**
 * Created by Arnold on 2016-05-08.
 */
public enum AlgorithmType {
    SUPPORT_VECTOR_MACHINE, K_NEAREST_NEIGHBOUR, NEAREST_MEAN, FFNEURALNETWORK;
}
