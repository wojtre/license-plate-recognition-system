package numberPlateSegmentation.machineLearning.algorithmCreatorApp;

import javax.swing.*;
import java.awt.*;

/**
 * Created by Arnold on 2016-05-09.
 */
public class AlgorithmCreator {
    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                JFrame frame = new AlgorithmCreatorFrame();
                frame.setTitle("Algorithm Creator");
                frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                frame.setVisible(true);
            }
        });
    }
}
