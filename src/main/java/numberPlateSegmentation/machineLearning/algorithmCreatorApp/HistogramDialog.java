package numberPlateSegmentation.machineLearning.algorithmCreatorApp;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.DefaultCategoryDataset;

import javax.swing.*;
import java.util.List;

/**
 * Created by Arnold on 2016-05-09.
 */
public class HistogramDialog extends JDialog {
    JFreeChart chart;
    public HistogramDialog(List<String> labels, double[] quantities) {
        super();
        this.setTitle("Histogram");
        this.setSize(800, 600);
        this.createChart(labels,quantities);

        ChartPanel chartPanel = new ChartPanel(chart);
        this.add(chartPanel);

        this.setVisible(true);
    }
    private void createChart(List<String> labels, double[] quantities){

        DefaultCategoryDataset dataSet = new DefaultCategoryDataset();
        for (int i = 0; i < labels.size(); i++) {
            dataSet.setValue(quantities[i],"quantity",labels.get(i));
        }
        chart = ChartFactory.createBarChart("signs histogram", "signs", "quantity", dataSet, PlotOrientation.VERTICAL, true, true, false);

    }
}
