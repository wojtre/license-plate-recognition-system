package numberPlateSegmentation.machineLearning.algorithmCreatorApp;

import numberPlateSegmentation.machineLearning.enums.AlgorithmType;
import numberPlateSegmentation.machineLearning.impl.*;
import numberPlateSegmentation.machineLearning.interfaces.BasicAlgorithmService;
import numberPlateSegmentation.machineLearning.interfaces.ImageContainerService;
import org.encog.ml.svm.KernelType;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.*;
import java.util.List;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;

/**
 * Created by Arnold on 2016-05-09.
 */
public class AlgorithmCreatorFrame extends JFrame {
    private static int DEFAULT_WIDTH = 800;
    private static int DEFAULT_HEIGHT = 600;

    private JTextArea txtAreaDisplayer;
    private JComboBox<AlgorithmType> comboBoxAlgorithmList;
    private JButton btnLoadData;
    private JButton btnDisplayHistogram;
    private JButton btnTeachAlgorithm;
    private JButton btnSaveAlgorithm;
    private JButton btnCrossValidation;
    private JTextField txtFieldCrossValidation;
    private JDialog dialogHistogram;
    private JFileChooser fileChooser;

    private ImageContainerService dataContainer;
    private AlgorithmType algorithmType = AlgorithmType.SUPPORT_VECTOR_MACHINE;
    private BasicAlgorithmService basicAlgorithm = null;

    public AlgorithmCreatorFrame() {
        super();
        this.setSize(DEFAULT_WIDTH, DEFAULT_HEIGHT);
        initializeFrame();
    }

    private void initializeFrame() {
        txtAreaDisplayer = new JTextArea(40, 10);
        this.add(new JScrollPane(txtAreaDisplayer), BorderLayout.CENTER);
        this.createFileChooser("pliki csv i xml", "xml", "csv");
        this.createEastPanel();
    }

    private void createEastPanel() {
        JPanel mainPanel = new JPanel(new GridLayout(5, 1));
        JPanel listPanel = new JPanel();
        JPanel loadPanel = new JPanel();
        JPanel displayPanel = new JPanel();
        JPanel validationPanel = new JPanel();
        JPanel teachLoadPanel = new JPanel();

        this.createComboBoxAlgorithmList();
        this.btnLoadData = new JButton("Load data");
        this.btnDisplayHistogram = new JButton("display Histogram");
        this.btnCrossValidation = new JButton("crossValidation");
        this.btnSaveAlgorithm = new JButton("Serialize");
        this.btnTeachAlgorithm = new JButton("Train Classifier");


        listPanel.add(this.comboBoxAlgorithmList);
        loadPanel.add(btnLoadData);
        displayPanel.add(btnDisplayHistogram);
        validationPanel.add(btnCrossValidation);
        teachLoadPanel.add(btnTeachAlgorithm);
        teachLoadPanel.add(btnSaveAlgorithm);

        mainPanel.add(listPanel);
        mainPanel.add(loadPanel);
        mainPanel.add(displayPanel);
        mainPanel.add(validationPanel);
        mainPanel.add(teachLoadPanel);

        this.initializeButtonState();

        this.add(mainPanel, BorderLayout.EAST);
    }

    private void createComboBoxAlgorithmList() {
        this.comboBoxAlgorithmList = new JComboBox<>();
        this.comboBoxAlgorithmList.addItem(AlgorithmType.SUPPORT_VECTOR_MACHINE);
        this.comboBoxAlgorithmList.addItem(AlgorithmType.K_NEAREST_NEIGHBOUR);
        this.comboBoxAlgorithmList.addItem(AlgorithmType.NEAREST_MEAN);

        this.comboBoxAlgorithmList.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                algorithmType = comboBoxAlgorithmList.getItemAt(comboBoxAlgorithmList.getSelectedIndex());
                txtAreaDisplayer.append('\n' + "Selected algorithm: " + algorithmType + '\n');
            }
        });
    }

    private void initializeButtonState() {
        this.btnCrossValidation.setEnabled(false);
        this.btnDisplayHistogram.setEnabled(false);
        this.btnSaveAlgorithm.setEnabled(false);
        this.btnTeachAlgorithm.setEnabled(false);

        this.btnLoadData.addActionListener(new LoadDataAction());
        this.btnCrossValidation.addActionListener(new CrossValidationAction());
        this.btnDisplayHistogram.addActionListener(new DisplayHistogramAction());
        this.btnSaveAlgorithm.addActionListener(new SaveAlgorithmAction());
        this.btnTeachAlgorithm.addActionListener(new TeachAlgorithmAction());
    }

    private void enableButtons() {
        this.btnCrossValidation.setEnabled(true);
        this.btnDisplayHistogram.setEnabled(true);
        this.btnTeachAlgorithm.setEnabled(true);
    }

    private void createFileChooser(String description, String... args) {
        this.fileChooser = new JFileChooser(new File("."));
        FileNameExtensionFilter filter = new FileNameExtensionFilter(description, args);
        this.fileChooser.setFileFilter(filter);
        this.fileChooser.setAcceptAllFileFilterUsed(false);
        this.fileChooser.setMultiSelectionEnabled(false);
    }

    private class LoadDataAction implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            int answer = fileChooser.showOpenDialog(null);
            if (answer == JFileChooser.APPROVE_OPTION) {
                File file = fileChooser.getSelectedFile();

                try {
                    dataContainer = new ImageInfoContainer(file.getAbsolutePath());
                    txtAreaDisplayer.append("Data Container is loaded\n");
                    enableButtons();
                } catch (IOException e1) {
                    txtAreaDisplayer.append("exception while loading file..." + '\n');
                    e1.printStackTrace();
                }

            }

        }
    }

    private class DisplayHistogramAction implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            List<String> labels = dataContainer.getHistogramLabels();
            double[] quantities = dataContainer.getHistogramCounts();
            int thrashPosition = this.thrashElementPosition(labels);
            if (thrashPosition < 0) {
                new HistogramDialog(labels, quantities);
            } else {
                String thrashElement = labels.get(thrashPosition);
                double[] quantitiesFirstGroup = new double[thrashPosition];
                double[] quantitiesSecondGroup = new double[quantities.length - thrashPosition];
                List<String> labelsFirstGroup = labels.subList(0, thrashPosition);
                List<String> labelsSecondGroup = labels.subList(thrashPosition, quantities.length);
                System.arraycopy(quantities, 0, quantitiesFirstGroup, 0, thrashPosition);
                System.arraycopy(quantities, thrashPosition, quantitiesSecondGroup, 0, quantities.length - thrashPosition);
                new HistogramDialog(labelsFirstGroup, quantitiesFirstGroup);
                new HistogramDialog(labelsSecondGroup, quantitiesSecondGroup);
            }
        }

        private int thrashElementPosition(List<String> label) {
            for (int i = 0; i < label.size(); i++) {
                if (label.get(i).length() > 1) {
                    return i;
                }
            }
            return -1;
        }
    }


    private class CrossValidationAction implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            String result = AlgorithmValidation.crossValidation(dataContainer, algorithmType, 5);
            txtAreaDisplayer.append(result);
            String result2 = AlgorithmValidation.confusionMatrixResult(dataContainer,algorithmType,5);
            txtAreaDisplayer.append(result2);
        }
    }

    private class TeachAlgorithmAction implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            if (algorithmType.equals(AlgorithmType.SUPPORT_VECTOR_MACHINE)) {
                basicAlgorithm = new SvmClassifier(dataContainer, KernelType.Linear, 1);
                txtAreaDisplayer.append(algorithmType + " classifier created\n");
                btnSaveAlgorithm.setEnabled(true);
            }
            if (algorithmType.equals(AlgorithmType.K_NEAREST_NEIGHBOUR)) {
                basicAlgorithm = new kNearestNeighbour(dataContainer);
                txtAreaDisplayer.append(algorithmType + " classifier created\n");
                btnSaveAlgorithm.setEnabled(true);
            }
            if (algorithmType.equals(AlgorithmType.NEAREST_MEAN)) {
                basicAlgorithm = new NearestMean(dataContainer);
                txtAreaDisplayer.append(algorithmType + " classifier created\n");
                btnSaveAlgorithm.setEnabled(true);
            }

        }
    }

    private class SaveAlgorithmAction implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            try {
                String fileName = algorithmType + ".ser";
                basicAlgorithm.save(fileName);
                txtAreaDisplayer.append("saved as: " + fileName);
            } catch (IOException ex) {
                txtAreaDisplayer.append("errors while serializing\n");
            }
        }
    }


}
