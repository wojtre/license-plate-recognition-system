/**
 * Created by Wojciech on 05-Oct-15.
 */

import client.MainWindowController;
import client.MainWindowModel;
import client.MainWindowView;

import org.opencv.core.Mat;
import org.opencv.highgui.Highgui;

import java.awt.*;
import java.io.File;
import java.io.IOException;

public class Main {

    public static final String OPENCV_PATH_WOJTEK = "C:\\Program Files\\Java\\opencv\\build\\java\\x64\\opencv_java2411.dll";
    public static final String OPENCV_PATH_ARNI = "C:\\opencv\\build\\java\\x64\\opencv_java2411.dll";

    public static void main(String[] args) throws IOException {
        System.load(OPENCV_PATH_ARNI);
        startApplication();
    }

    private static void startApplication() {
        EventQueue.invokeLater(() -> {
            try {
                MainWindowView mainWindowView = new MainWindowView();
                MainWindowModel mainWindowModel = new MainWindowModel();
                new MainWindowController(mainWindowModel, mainWindowView);
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }

    public static Mat getImageFromResources(String path) throws IOException {
        String canonicalPath = new File(".").getCanonicalPath();
        String replace = canonicalPath.replace("\\", "\\\\");
        return Highgui.imread(replace + "\\src\\main\\resources\\" + path, Highgui.CV_LOAD_IMAGE_GRAYSCALE);
    }
}
