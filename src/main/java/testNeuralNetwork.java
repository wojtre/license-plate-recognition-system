import numberPlateSegmentation.machineLearning.enums.AlgorithmType;
import numberPlateSegmentation.machineLearning.impl.AlgorithmValidation;
import numberPlateSegmentation.machineLearning.impl.ImageInfoContainer;
import numberPlateSegmentation.machineLearning.interfaces.ImageContainerService;

/**
 * Created by Arnold on 2016-06-04.
 */
public class testNeuralNetwork {
    public static void main(String[] args) throws Exception {
        ImageContainerService container = null;
        try {
            String CSVFile = "C:\\IdeaProjects\\studia\\ZMiTAD_projekt\\src\\main\\resources\\dataSets\\letter-recognition.csv";
            String XMLFile = "C:\\IdeaProjects\\EngineeringProject\\AlgorythmImplementations\\data\\xmlDataInfo.xml";
            container = new ImageInfoContainer(CSVFile);
        } catch (Exception e) {
            e.printStackTrace();
        }
        //FFNeuralNetwork neuralNetwork = new FFNeuralNetwork(container, 32);
        //System.out.println(100.0D * AlgorithmValidation.validate(container,neuralNetwork));
        /**
         * Tutaj pierwszych czterech parametrów najlepiej nie zmieniaj, ostatni czyli 3 oznacza że krosswalidacja
         * przeprowadzana jest na trzech zbiorach, po tej liczbie, możesz dawac int... neuronow w warstwach ukrytych
         */
        System.out.println(AlgorithmValidation.crossValidation(container, AlgorithmType.FFNEURALNETWORK, 3, 32));

        System.exit(0);
    }
}
