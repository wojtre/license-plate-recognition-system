package client;

import numberPlateSegmentation.imageProcessing.impl.SegmentationResultRetrievingServiceImpl;
import numberPlateSegmentation.imageProcessing.interfaces.SegmentationResultRetrievingService;

import numberPlateSegmentation.machineLearning.impl.LicensePlateRecognition;
import numberPlateSegmentation.videoProcessing.impl.FrameCapture;
import numberPlateSegmentation.videoProcessing.impl.FrameCaptureListener;
import org.opencv.core.*;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import utils.ImageUtils;

import javax.swing.*;
import javax.swing.event.ChangeListener;
import java.awt.*;
import java.awt.event.*;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.util.List;

import static client.PropertyID.IMAGE;

/**
 * Created by wojci on 02.04.2016.
 */
public class MainWindowController implements PropertyChangeListener {
    private static final int PLATE_SIZE = 150;

    private MainWindowModel model;
    private MainWindowView view;
    private SegmentationResultRetrievingService segmentationResultRetrievingService = new SegmentationResultRetrievingServiceImpl();

    private ActionListener startStopButtonListener;
    private ActionListener fileChooseButtonListener;
    private ChangeListener videoSpeedSliderListener;
    private MouseListener roiMouseListener;
    private MouseMotionListener roiMouseMotionListener;

    private Thread video = null;
    private FrameCapture frameCapture = null;

    public MainWindowController(MainWindowModel model, MainWindowView view) {
        this.model = model;
        this.view = view;

        this.model.addListener(this);
        initializeListeners();
    }

    private void initializeListeners() {
        setStartStopButtonListener();
        setFileChooseButtonListener();
        setMouseListeners();
        setVideoSpeedSliderListener();
    }

    private void setStartStopButtonListener() {
        startStopButtonListener = e -> {
            if (model.isStartStop()) {
                pushedStopReaction();
            } else {
                pushedStartReaction();
            }
        };
        view.getBtnPlayStop().addActionListener(startStopButtonListener);
    }

    private void pushedStartReaction() {
        model.setStartStop(true);
        view.getBtnPlayStop().setText("Stop");
        if (model.isNewFilm()) {
            if (video != null) {
                video.resume();
                frameCapture.terminate();
                try {
                    video.join();
                } catch (InterruptedException e1) {
                    e1.printStackTrace();
                }
            }
            frameCapture = new FrameCapture(model.getFileName());
            frameCapture.addListener(getFrameCaptureListener());
            video = new Thread(frameCapture);
            video.start();

        } else {
            if (video != null) {
                video.resume();
            }
        }
        model.setNewFilm(false);
    }

    private FrameCaptureListener getFrameCaptureListener() {
        return image -> {
            try {
                Thread.sleep(model.getDelayTimeInMs());
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            ResultTO resultTO = segmentationResultRetrievingService.segmentSigns(image, model.getRegionOfInterest());
            model.setImage(image);
            for (Mat plate : resultTO.getNormalizedPlates()) {
                model.addPlate(plate);
            }
            for (List<Mat> signs : resultTO.getSegmentedSigns()) {
                model.addSigns(signs);
            }
            model.setFoundPlates(resultTO.getFoundPlates());
            List<String> recognizePlates = LicensePlateRecognition.recognizePlate(resultTO.getSegmentedSigns(),
                    model.getAlgorithm(), 20, 20);
            for(String plate : recognizePlates) {
                model.addRecognizedPlate(plate);
            }
        };
    }

    private void pushedStopReaction() {
        model.setStartStop(false);
        view.getBtnPlayStop().setText("Play");
        if (video != null) {
            video.suspend();
        }
    }

    private void setFileChooseButtonListener() {
        fileChooseButtonListener = e -> {
            JFileChooser fileChooser = new JFileChooser();
            fileChooser.setCurrentDirectory(new File(System.getProperty("user.home")));
            try {
                fileChooser.showOpenDialog(null);
                File f = fileChooser.getSelectedFile();
                model.setFileName(f.getAbsolutePath());
                view.getTextFile().setText(f.getAbsolutePath());
            } catch (Exception ex) {
                // noop
            }
        };
        view.getBtnFile().addActionListener(fileChooseButtonListener);
    }

    private void setMouseListeners() {
        roiMouseListener = new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {
                model.setStartDrag(new Point(e.getX(), e.getY()));
                model.setEndDrag(new Point(e.getX(), e.getY()));
                model.setRegionOfInterest(null);
            }

            @Override
            public void mouseReleased(MouseEvent e) {
                model.setRegionOfInterest(new Rect(model.getStartDrag(), new Point(e.getX(), e.getY())));
            }
        };
        roiMouseMotionListener = new MouseMotionAdapter() {
            @Override
            public void mouseDragged(MouseEvent e) {
                model.setEndDrag(new Point(e.getX(), e.getY()));
            }
        };
        view.getLabelVideo().addMouseListener(roiMouseListener);
        view.getLabelVideo().addMouseMotionListener(roiMouseMotionListener);
    }

    private void setVideoSpeedSliderListener() {
        videoSpeedSliderListener = e -> model.setDelayTimeInMs(((JSlider) e.getSource()).getValue());
        view.getVideoSpeedSlide().addChangeListener(videoSpeedSliderListener);
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        String propName = evt.getPropertyName();
        Object newVal = evt.getNewValue();

        if (IMAGE.equalsIgnoreCase(propName)) {
            Image newImage = (Image) newVal;
            if (newImage != null) {
                if (model.getRegionOfInterest() != null) {
                }
                ImageUtils.displayImageOnFrame(newImage, view.getPanelVideo(), view.getLabelVideo());
            }
        } else if (PropertyID.SIGNS_1.equals(propName)) {
            updateSigns((List<Mat>) newVal, view.getSigns1());
        } else if (PropertyID.SIGNS_2.equals(propName)) {
            updateSigns((List<Mat>) newVal, view.getSigns2());
        } else if (PropertyID.SIGNS_3.equals(propName)) {
            updateSigns((List<Mat>) newVal, view.getSigns3());
        } else if (PropertyID.PLATE_1.equals(propName)) {
            updatePlate((Mat) newVal, view.getLblPlate1());
        } else if (PropertyID.PLATE_2.equals(propName)) {
            updatePlate((Mat) newVal, view.getLblPlate2());
        } else if (PropertyID.PLATE_3.equals(propName)) {
            updatePlate((Mat) newVal, view.getLblPlate3());
        } else if(PropertyID.REC_PLATE1.equals(propName)){
            view.getLblRecognizedPlate1().setText((String) newVal);
        }else if(PropertyID.REC_PLATE2.equals(propName)){
            view.getLblRecognizedPlate2().setText((String) newVal);
        }else if(PropertyID.REC_PLATE3.equals(propName)){
            view.getLblRecognizedPlate3().setText((String) newVal);
        }

    }

    private void updatePlate(Mat plate, JLabel plateFromView) {
        if (plate != null) {
            ImageUtils.resizeImage(plate, PLATE_SIZE);
            ImageUtils.displayImageOnFrame2(ImageUtils.mat2BufferedImage(plate), view.getFrame(), plateFromView);
        }
    }

    private void updateSigns(List<Mat> newVal, List<JLabel> signsFromView) {
        List<Mat> signs = newVal;
        if (signs != null) {
            for (int i = 0; i < signsFromView.size(); i++){
                signsFromView.get(i).setIcon(null);
            }
            for (int j = 0; j < signs.size(); j++) {
                ImageUtils.displayImageOnFrame2(ImageUtils.mat2BufferedImage(signs.get(j)), view.getFrame(), signsFromView.get(j));
            }
        }
    }
}
