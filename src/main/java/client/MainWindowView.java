package client;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by wojci on 02.04.2016.
 */
public class MainWindowView {
    private JFrame frame;
    private JPanel panelVideo;
    private JButton btnPlayStop;
    private JLabel lblOstatnioWydzieloneTablice;
    private JLabel lblWydzieloneZnaki;
    private JLabel lblRozpoznaneZnaki;
    private JLabel lblPlate1;
    private JLabel lblPlate2;
    private JLabel lblPlate3;
    private JLabel lblRecognizedPlate1;
    private JLabel lblRecognizedPlate2;
    private JLabel lblRecognizedPlate3;
    private JButton btnFile;
    private JTextField textFile;
    private java.util.List<JLabel> signs1;
    private java.util.List<JLabel> signs2;
    private java.util.List<JLabel> signs3;
    private JSlider videoSpeedSlide;
    private JLabel lblSlider;
    private JLabel labelVideo;
    /**
     * Test view look.
     */
    public static void main(String[] args) {
        EventQueue.invokeLater(() -> {
            try {
                MainWindowView window = new MainWindowView();
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }

    public MainWindowView() {
        initialize();
    }

    private void initialize() {
        initializeFrame();
        addPanelVideo();
        addVideoLabel();
        addBtnPlayStop();
        addLblWydzieloneTablice();
        addLblWydzieloneZnaki();
        addLblRozpoznaneZnaki();
        addRecognizedPlates();
        addPlates();
        addBtnFileChoose();
        addTxtFile();
        addSigns();
        addVideoSpeedSlider();
        addLblSlider();
        frame.setVisible(true);
    }

    private void addVideoLabel(){
        labelVideo = new JLabel();
        panelVideo.add(labelVideo);
    }

    private void addSigns() {
        signs1 = new ArrayList<JLabel>();
        signs2 = new ArrayList<JLabel>();
        signs3 = new ArrayList<JLabel>();
        for (int i = 0; i < 9; i++) {
            JLabel jLabel1 = new JLabel();
            jLabel1.setBounds(890 + i * 40, 400, 40, 40);
            signs1.add(jLabel1);
            frame.add(jLabel1);
            JLabel jLabel2 = new JLabel();
            jLabel2.setBounds(890 + i * 40, 460, 40, 40);
            signs2.add(jLabel2);
            frame.add(jLabel2);

            JLabel jLabel3 = new JLabel();
            jLabel3.setBounds(890 + i * 40, 520, 40, 40);
            signs3.add(jLabel3);
            frame.add(jLabel3);

        }
    }

    private void addRecognizedPlates() {
        lblRecognizedPlate1 = new JLabel();
        lblRecognizedPlate2 = new JLabel();
        lblRecognizedPlate3 = new JLabel();

        lblRecognizedPlate1.setBounds(1210, 400, 150, 40);
        lblRecognizedPlate2.setBounds(1210, 460, 150, 40);
        lblRecognizedPlate3.setBounds(1210, 520, 150, 40);

        lblRecognizedPlate1.setFont(new Font("Tahoma", Font.BOLD, 14));
        lblRecognizedPlate2.setFont(new Font("Tahoma", Font.BOLD, 14));
        lblRecognizedPlate3.setFont(new Font("Tahoma", Font.BOLD, 14));

        frame.add(lblRecognizedPlate1);
        frame.add(lblRecognizedPlate2);
        frame.add(lblRecognizedPlate3);
    }

    private void addPlates() {
        lblPlate1 = new JLabel();
        lblPlate1.setBounds(890, 270, 251, 70);
        frame.getContentPane().add(lblPlate1);

        lblPlate2 = new JLabel();
        lblPlate2.setBounds(1050, 270, 251, 70);
        frame.getContentPane().add(lblPlate2);

        lblPlate3 = new JLabel();
        lblPlate3.setBounds(1210, 270, 251, 70);
        frame.getContentPane().add(lblPlate3);
    }

    private void addLblWydzieloneZnaki() {
        lblWydzieloneZnaki = new JLabel("Wydzielone znaki");
        lblWydzieloneZnaki.setFont(new Font("Tahoma", Font.BOLD, 14));
        lblWydzieloneZnaki.setBounds(890, 350, 125, 32);
        frame.getContentPane().add(lblWydzieloneZnaki);
    }

    private void addLblRozpoznaneZnaki() {
        lblRozpoznaneZnaki = new JLabel("Rozpoznane znaki");
        lblRozpoznaneZnaki.setFont(new Font("Tahoma", Font.BOLD, 14));
        lblRozpoznaneZnaki.setBounds(1210, 350, 125, 32);
        frame.getContentPane().add(lblRozpoznaneZnaki);
    }

    private void addLblWydzieloneTablice() {
        lblOstatnioWydzieloneTablice = new JLabel(
                "Ostatnio wydzielone tablice");
        lblOstatnioWydzieloneTablice.setFont(new Font("Tahoma", Font.BOLD, 14));
        lblOstatnioWydzieloneTablice.setBounds(890, 240, 193, 14);
        frame.getContentPane().add(lblOstatnioWydzieloneTablice);
    }

    private void addBtnPlayStop() {
        btnPlayStop = new JButton("Play");
        btnPlayStop.setBounds(320, 712, 89, 30);
        frame.getContentPane().add(btnPlayStop);
    }

    private void addPanelVideo() {
        panelVideo = new JPanel();
        panelVideo.setBounds(24, 25, 731, 537);
        frame.getContentPane().add(panelVideo);
    }

    private void initializeFrame() {
        frame = new JFrame();
        frame.setBounds(100, 100, 1395, 803);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.getContentPane().setLayout(null);
        try {
            // Set System L&F
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (UnsupportedLookAndFeelException e) {
            // handle exception
        } catch (ClassNotFoundException e) {
            // handle exception
        } catch (InstantiationException e) {
            // handle exception
        } catch (IllegalAccessException e) {
            // handle exception
        }
    }

    private void addTxtFile() {
        textFile = new JTextField("Plik wideo");
        textFile.setBounds(890, 25, 250, 30);
        frame.getContentPane().add(textFile);
    }

    private void addBtnFileChoose() {
        btnFile = new JButton("Wybierz plik");
        btnFile.setBounds(1150, 25, 89, 30);
        frame.getContentPane().add(btnFile);
    }

    private void addLblSlider() {
        lblSlider = new JLabel("Predkosc filmu");
        lblSlider.setFont(new Font("Tahoma", Font.BOLD, 14));
        lblSlider.setBounds(890, 110, 200, 26);
        frame.add(lblSlider);
    }

    private void addVideoSpeedSlider() {
        videoSpeedSlide = new JSlider();
        videoSpeedSlide.setBounds(890, 150, 200, 26);
        videoSpeedSlide.setValue(30);
        videoSpeedSlide.setMaximum(250);
        videoSpeedSlide.setMinimum(10);
        videoSpeedSlide.setPaintTicks(true);
        frame.getContentPane().add(videoSpeedSlide);
    }

    public JFrame getFrame() {
        return frame;
    }

    public void setFrame(JFrame frame) {
        this.frame = frame;
    }

    public JPanel getPanelVideo() {
        return panelVideo;
    }

    public void setPanelVideo(JPanel panelVideo) {
        this.panelVideo = panelVideo;
    }

    public JButton getBtnPlayStop() {
        return btnPlayStop;
    }

    public void setBtnPlayStop(JButton btnPlayStop) {
        this.btnPlayStop = btnPlayStop;
    }

    public JLabel getLblOstatnioWydzieloneTablice() {
        return lblOstatnioWydzieloneTablice;
    }

    public void setLblOstatnioWydzieloneTablice(JLabel lblOstatnioWydzieloneTablice) {
        this.lblOstatnioWydzieloneTablice = lblOstatnioWydzieloneTablice;
    }

    public JLabel getLblWydzieloneZnaki() {
        return lblWydzieloneZnaki;
    }

    public void setLblWydzieloneZnaki(JLabel lblWydzieloneZnaki) {
        this.lblWydzieloneZnaki = lblWydzieloneZnaki;
    }

    public JLabel getLblPlate1() {
        return lblPlate1;
    }

    public void setLblPlate1(JLabel lblPlate1) {
        this.lblPlate1 = lblPlate1;
    }

    public JLabel getLblPlate2() {
        return lblPlate2;
    }

    public void setLblPlate2(JLabel lblPlate2) {
        this.lblPlate2 = lblPlate2;
    }

    public JLabel getLblPlate3() {
        return lblPlate3;
    }

    public void setLblPlate3(JLabel lblPlate3) {
        this.lblPlate3 = lblPlate3;
    }

    public JButton getBtnFile() {
        return btnFile;
    }

    public void setBtnFile(JButton btnFile) {
        this.btnFile = btnFile;
    }

    public JTextField getTextFile() {
        return textFile;
    }

    public void setTextFile(JTextField textFile) {
        this.textFile = textFile;
    }

    public java.util.List<JLabel> getSigns1() {
        return signs1;
    }

    public void setSigns1(List<JLabel> signs1) {
        this.signs1 = signs1;
    }

    public List<JLabel> getSigns2() {
        return signs2;
    }

    public void setSigns2(List<JLabel> signs2) {
        this.signs2 = signs2;
    }

    public List<JLabel> getSigns3() {
        return signs3;
    }

    public void setSigns3(List<JLabel> signs3) {
        this.signs3 = signs3;
    }

    public JSlider getVideoSpeedSlide() {
        return videoSpeedSlide;
    }

    public void setVideoSpeedSlide(JSlider videoSpeedSlide) {
        this.videoSpeedSlide = videoSpeedSlide;
    }

    public JLabel getLblSlider() {
        return lblSlider;
    }

    public void setLblSlider(JLabel lblSlider) {
        this.lblSlider = lblSlider;
    }

    public JLabel getLabelVideo() {
        return labelVideo;
    }

    public void setLabelVideo(JLabel labelVideo) {
        this.labelVideo = labelVideo;
    }

    public JLabel getLblRecognizedPlate1() {
        return lblRecognizedPlate1;
    }

    public void setLblRecognizedPlate1(JLabel lblRecognizedPlate1) {
        this.lblRecognizedPlate1 = lblRecognizedPlate1;
    }

    public JLabel getLblRecognizedPlate2() {
        return lblRecognizedPlate2;
    }

    public void setLblRecognizedPlate2(JLabel lblRecognizedPlate2) {
        this.lblRecognizedPlate2 = lblRecognizedPlate2;
    }

    public JLabel getLblRecognizedPlate3() {
        return lblRecognizedPlate3;
    }

    public void setLblRecognizedPlate3(JLabel lblRecognizedPlate3) {
        this.lblRecognizedPlate3 = lblRecognizedPlate3;
    }
}
