package client;


import numberPlateSegmentation.machineLearning.impl.SvmClassifier;
import numberPlateSegmentation.machineLearning.interfaces.BasicAlgorithmService;
import org.opencv.core.Mat;
import org.opencv.core.Point;
import org.opencv.core.Rect;

import javax.swing.event.SwingPropertyChangeSupport;
import java.awt.image.BufferedImage;
import java.beans.PropertyChangeListener;
import java.io.IOException;
import java.util.List;

import static client.PropertyID.*;
import static client.PropertyID.IMAGE;
import static client.PropertyID.SIGNS_3;

/**
 * Created by wojci on 02.04.2016.
 */
public class MainWindowModel {
    private boolean startStop;
    private String fileName;
    private Point startDrag;
    private Point endDrag;
    private Rect regionOfInterest;
    private BufferedImage image;
    private long delayTimeInMs = 35;
    private boolean newFilm;
    private Mat plate1;
    private Mat plate2;
    private Mat plate3;
    private List<Mat> signs1;
    private List<Mat> signs2;
    private List<Mat> signs3;
    private List<Rect> foundPlates;
    private BasicAlgorithmService algorithm;
    private String recognizedPlate1;
    private String recognizedPlate2;
    private String recognizedPlate3;


    private SwingPropertyChangeSupport propChangeFirer;

    public MainWindowModel() {
        propChangeFirer = new SwingPropertyChangeSupport(this);
        try {
            algorithm = SvmClassifier.load("SUPPORT_VECTOR_MACHINE.ser");
        } catch (IOException e) {

        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    public void addPlate(Mat plate) {
        setPlate3(plate2);
        setPlate2(plate1);
        setPlate1(plate);
    }

    public void addSigns(List<Mat> signs) {
        setSigns3(signs2);
        setSigns2(signs1);
        setSigns1(signs);
    }

    public void addRecognizedPlate(String plate) {
        setRecognizedPlate3(recognizedPlate2);
        setRecognizedPlate2(recognizedPlate1);
        setRecognizedPlate1(plate);
    }

    public void addListener(PropertyChangeListener prop) {
        propChangeFirer.addPropertyChangeListener(prop);
    }

    public boolean isStartStop() {
        return startStop;
    }

    public void setStartStop(boolean startStop) {
        this.startStop = startStop;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        newFilm = true;
        this.fileName = fileName;
    }

    public Point getStartDrag() {
        return startDrag;
    }

    public void setStartDrag(Point startDrag) {
        this.startDrag = startDrag;
    }

    public Point getEndDrag() {
        return endDrag;
    }

    public void setEndDrag(Point endDrag) {
        this.endDrag = endDrag;
    }

    public Rect getRegionOfInterest() {
        return regionOfInterest;
    }

    public void setRegionOfInterest(Rect regionOfInterest) {
        this.regionOfInterest = regionOfInterest;
    }

    public BufferedImage getImage() {
        return image;
    }

    public void setImage(BufferedImage image) {
        BufferedImage old = this.image;
        this.image = image;
        propChangeFirer.firePropertyChange(IMAGE, old, image);
    }

    public long getDelayTimeInMs() {
        return delayTimeInMs;
    }

    public void setDelayTimeInMs(long delayTimeInMs) {
        this.delayTimeInMs = delayTimeInMs;
    }


    public boolean isNewFilm() {
        return newFilm;
    }

    public void setNewFilm(boolean newFilm) {
        this.newFilm = newFilm;
    }

    public Mat getPlate1() {
        return plate1;
    }

    public void setPlate1(Mat plate1) {
        Mat old = this.plate1;
        this.plate1 = plate1;
        propChangeFirer.firePropertyChange(PLATE_1, old, plate1);
    }

    public Mat getPlate2() {
        return plate2;
    }

    public void setPlate2(Mat plate2) {
        Mat old = this.plate2;
        this.plate2 = plate2;
        propChangeFirer.firePropertyChange(PLATE_2, old, plate2);
    }

    public Mat getPlate3() {
        return plate3;
    }

    public void setPlate3(Mat plate3) {
        Mat old = this.plate3;
        this.plate3 = plate3;
        propChangeFirer.firePropertyChange(PLATE_3, old, plate3);
    }

    public List<Mat> getSigns1() {
        return signs1;
    }

    public void setSigns1(List<Mat> signs1) {
        List<Mat> old = this.signs1;
        this.signs1 = signs1;
        propChangeFirer.firePropertyChange(SIGNS_1, old, signs1);
    }

    public List<Mat> getSigns2() {
        return signs2;

    }

    public void setSigns2(List<Mat> signs2) {
        List<Mat> old = this.signs2;
        this.signs2 = signs2;
        propChangeFirer.firePropertyChange(SIGNS_2, old, signs2);
    }

    public List<Mat> getSigns3() {
        return signs3;
    }

    public void setSigns3(List<Mat> signs3) {
        List<Mat> old = this.signs3;
        this.signs3 = signs3;
        propChangeFirer.firePropertyChange(SIGNS_3, old, signs3);
    }

    public List<Rect> getFoundPlates() {
        return foundPlates;
    }

    public void setFoundPlates(List<Rect> foundPlates) {
        this.foundPlates = foundPlates;
    }

    public BasicAlgorithmService getAlgorithm() {
        return algorithm;
    }

    public void setAlgorithm(BasicAlgorithmService algorithm) {
        this.algorithm = algorithm;
    }

    public String getRecognizedPlate1() {
        return recognizedPlate1;
    }

    public void setRecognizedPlate1(String recognizedPlate1) {
        String old = this.recognizedPlate1;
        this.recognizedPlate1 = recognizedPlate1;
        propChangeFirer.firePropertyChange(PropertyID.REC_PLATE1, old, recognizedPlate1);
    }

    public String getRecognizedPlate2() {
        return recognizedPlate2;
    }

    public void setRecognizedPlate2(String recognizedPlate2) {
        String old = this.recognizedPlate2;
        this.recognizedPlate2 = recognizedPlate2;
        propChangeFirer.firePropertyChange(PropertyID.REC_PLATE2, old, recognizedPlate2);

    }

    public String getRecognizedPlate3() {
        return recognizedPlate3;
    }

    public void setRecognizedPlate3(String recognizedPlate3) {
        String old = this.recognizedPlate3;
        this.recognizedPlate3 = recognizedPlate3;
        propChangeFirer.firePropertyChange(PropertyID.REC_PLATE3, old, recognizedPlate3);

    }
}
