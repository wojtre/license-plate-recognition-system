package client;

/**
 * Created by wojci on 03.04.2016.
 */
interface PropertyID {
    String IMAGE = "image";
    String SIGNS_1 = "signs1";
    String SIGNS_2 = "signs2";
    String SIGNS_3 = "signs3";
    String PLATE_1 = "plate1";
    String PLATE_2 = "plate2";
    String PLATE_3 = "plate3";
    String REC_PLATE1 = "recPlate1";
    String REC_PLATE2 = "recPlate2";
    String REC_PLATE3 = "recPlate3";
}
