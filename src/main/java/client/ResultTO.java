package client;

import org.opencv.core.Mat;
import org.opencv.core.Rect;

import java.util.List;

/**
 * Created by wojci on 03.04.2016.
 */
public class ResultTO {
    private List<List<Mat>> segmentedSigns;
    private List<Rect> foundPlates;
    private List<Mat> normalizedPlates;

    public List<List<Mat>> getSegmentedSigns() {
        return segmentedSigns;
    }

    public void setSegmentedSigns(List<List<Mat>> segmentedSigns) {
        this.segmentedSigns = segmentedSigns;
    }

    public List<Rect> getFoundPlates() {
        return foundPlates;
    }

    public void setFoundPlates(List<Rect> foundPlates) {
        this.foundPlates = foundPlates;
    }

    public List<Mat> getNormalizedPlates() {
        return normalizedPlates;
    }

    public void setNormalizedPlates(List<Mat> normalizedPlates) {
        this.normalizedPlates = normalizedPlates;
    }
}