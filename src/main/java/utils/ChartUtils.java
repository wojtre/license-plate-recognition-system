package utils;

/**
 * Created by Wojciech on 05-Oct-15.
 */

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

import java.io.File;
import java.io.IOException;

public class ChartUtils {
    public static void printProjectionXFromArray(int[] data) {
        // Create a simple XY chart
        XYSeries series = new XYSeries("XYGraph");
        for (int i = 0; i < data.length; i++) {
            series.add(i, data[i]);

        }

        XYSeriesCollection dataset = new XYSeriesCollection();
        dataset.addSeries(series);
        JFreeChart chart = ChartFactory.createXYAreaChart("XY Chart", // Title
                "x-axis", // x-axis Label
                "y-axis", // y-axis Label
                dataset, // Dataset
                PlotOrientation.VERTICAL, // Plot Orientation
                false, // Show Legend
                false, // Use tooltips
                false // Configure chart to generate URLs?
        );
        try {
            ChartUtilities.saveChartAsJPEG(new File("chart.jpg"), chart, 700,
                    500);
        } catch (IOException e) {
            System.err.println("Problem occurred creating chart.");
        }
    }

    public static void printProjectionXFromArray(long[] data) {
        // Create a simple XY chart
        XYSeries series = new XYSeries("XYGraph");
        for (int i = 0; i < data.length; i++) {
            series.add(i, data[i]);

        }

        XYSeriesCollection dataset = new XYSeriesCollection();
        dataset.addSeries(series);
        JFreeChart chart = ChartFactory.createXYAreaChart("XY Chart", // Title
                "x-axis", // x-axis Label
                "y-axis", // y-axis Label
                dataset, // Dataset
                PlotOrientation.VERTICAL, // Plot Orientation
                false, // Show Legend
                false, // Use tooltips
                false // Configure chart to generate URLs?
        );
        try {
            ChartUtilities.saveChartAsJPEG(new File("chart.jpg"), chart, 700,
                    500);
        } catch (IOException e) {
            System.err.println("Problem occurred creating chart.");
        }
    }

    public static void printProjectionYFromArray(int[] data) {
        // Create a simple XY chart
        XYSeries series = new XYSeries("XYGraph");
        for (int i = 0; i < data.length; i++) {
            series.add(i, data[i]);
        }

        XYSeriesCollection dataset = new XYSeriesCollection();
        dataset.addSeries(series);
        JFreeChart chart = ChartFactory.createXYAreaChart("XY Chart", // Title
                "x-axis", // x-axis Label
                "y-axis", // y-axis Label
                dataset, // Dataset
                PlotOrientation.HORIZONTAL, // Plot Orientation
                true, // Show Legend
                true, // Use tooltips
                false // Configure chart to generate URLs?
        );
        try {
            ChartUtilities.saveChartAsJPEG(new File("chart2.jpg"), chart, 500,
                    700);
        } catch (IOException e) {
            System.err.println("Problem occurred creating chart.");
        }
    }

    public static void printProjectionYFromArray(long[] data) {
        // Create a simple XY chart
        XYSeries series = new XYSeries("XYGraph");
        for (int i = 0; i < data.length; i++) {
            series.add(i, data[data.length-1-i]);
        }

        XYSeriesCollection dataset = new XYSeriesCollection();
        dataset.addSeries(series);
        JFreeChart chart = ChartFactory.createXYAreaChart("XY Chart", // Title
                "x-axis", // x-axis Label
                "y-axis", // y-axis Label
                dataset, // Dataset
                PlotOrientation.HORIZONTAL, // Plot Orientation
                true, // Show Legend
                true, // Use tooltips
                false // Configure chart to generate URLs?
        );
        try {
            ChartUtilities.saveChartAsJPEG(new File("chart2.jpg"), chart, 500,
                  700);
        } catch (IOException e) {
            System.err.println("Problem occurred creating chart.");
        }
    }
}
