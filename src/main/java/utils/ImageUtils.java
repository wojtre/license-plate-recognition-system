package utils;

/**
 * Created by Wojciech on 05-Oct-15.
 */

import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.Point;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;

import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;

import static org.opencv.core.Core.line;

public class ImageUtils {
    public static Mat grayscaleToBin(Mat mat) {
        int size = (int) mat.total() * mat.channels();
        byte[] data = new byte[size];
        mat.get(0, 0, data);

        for (int i = 0; i < size; i++) {
            if (data[i] > 0) {
                data[i] = 0;
            } else {
                data[i] = (byte) 255;

            }
        }
        Mat bin = new Mat(mat.size(), CvType.CV_8U);
        bin.put(0, 0, data);
        return bin;
    }

    public static BufferedImage mat2BufferedImage(Mat m) {
        // source:
        // http://answers.opencv.org/question/10344/opencv-java-load-image-to-gui/
        // Fastest code
        // The output can be assigned either to a BufferedImage or to an Image

        int type = BufferedImage.TYPE_BYTE_GRAY;
        if (m.channels() > 1) {
            type = BufferedImage.TYPE_3BYTE_BGR;
        }
        int bufferSize = m.channels() * m.cols() * m.rows();
        byte[] b = new byte[bufferSize];
        m.get(0, 0, b); // get all the pixels
        BufferedImage image = new BufferedImage(m.cols(), m.rows(), type);
        final byte[] targetPixels = ((DataBufferByte) image.getRaster()
                .getDataBuffer()).getData();
        System.arraycopy(b, 0, targetPixels, 0, b.length);
        return image;

    }

    public static void displayImage(Image img2) {
        ImageIcon icon = new ImageIcon(img2);
        JFrame frame = new JFrame();
        frame.setLayout(new FlowLayout());
        frame.setSize(Math.max(img2.getWidth(null) + 100, 200), Math.max(img2.getHeight(null) + 50, 60));
        JLabel lbl = new JLabel();
        lbl.setIcon(icon);
        frame.add(lbl);
        frame.setVisible(true);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

    public static void displayImage(Mat image) {
        displayImage(mat2BufferedImage(image));
    }

    public static void displayImageOnFrame(Image img2, JFrame frame, JLabel lbl) {
        ImageIcon icon = new ImageIcon(img2);
        frame.setLayout(new FlowLayout());
        frame.setSize(Math.max(img2.getWidth(null) + 100, 200), Math.max(img2.getHeight(null) + 50, 60));
        lbl.setIcon(icon);
        frame.add(lbl);
        frame.setVisible(true);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.repaint();
        frame.validate();
        frame.getContentPane().repaint();
    }

    public static void displayImageOnFrame2(Image img2, JFrame frame, JLabel lbl) {
        ImageIcon icon = new ImageIcon(img2);
        lbl.setIcon(icon);
        frame.add(lbl);
        frame.setVisible(true);
        frame.repaint();
        frame.validate();
        frame.getContentPane().repaint();
    }

    public static void displayImageOnFrame(Image img2, JPanel panel, JLabel lbl) {
        ImageIcon icon = new ImageIcon(img2);
        panel.setLayout(new FlowLayout());
        panel.setSize(Math.max(img2.getWidth(null) + 100, 200), Math.max(img2.getHeight(null) + 50, 60));
        lbl.setIcon(icon);
        panel.setVisible(true);
        panel.repaint();
        panel.validate();
        panel.repaint();
    }


    public static Mat detectVerticalEdges(Mat image) {
        Mat dst = new Mat(image.rows(), image.cols(), image.type());
        Imgproc.Sobel(image, dst, image.type(), 1, 0, 3, 1, 0, Imgproc.BORDER_DEFAULT);
        return dst;
    }

    public static Mat detectEdges(Mat image) {
        Mat dst = new Mat(image.rows(), image.cols(), image.type());
        Imgproc.Sobel(image, dst, image.type(), 1, 1, 3, 1, 0, Imgproc.BORDER_DEFAULT);
        return dst;
    }

    public static Mat detectHorizontalEdges(Mat image) {
        Mat dst = new Mat(image.rows(), image.cols(), image.type());
        Imgproc.Sobel(image, dst, image.type(), 0, 1, 3, 1, 0, Imgproc.BORDER_DEFAULT);
        return dst;
    }

    public static Mat invertColours(Mat image) {
        Mat invertcolormatrix = new Mat(image.rows(), image.cols(), image.type(), new Scalar(255, 255, 255));
        Mat inverted = new Mat(image.rows(), image.cols(), image.type());
        Core.subtract(invertcolormatrix, image, inverted);
        return inverted;
    }

    public static Mat printHorizontalLines(Mat image, java.util.List<Integer> lines) {
        Mat imageWithDrawedLines = new Mat();
        Imgproc.cvtColor(image, imageWithDrawedLines, Imgproc.COLOR_GRAY2BGR);
        for (Integer line : lines) {
            if (line < image.rows() && line >= 0) {
                line(imageWithDrawedLines, new Point(0, line), new Point(image.cols() - 1, line), new Scalar(0, 0, 255), 1);
            }
        }
        return imageWithDrawedLines;

    }

    public static void resizeImage(Mat imageToResize, double width) {
        double ratio = countRatioOfImage(imageToResize);
        double height = ratio * width;
        Imgproc.resize(imageToResize, imageToResize, new Size(width, height));
    }

    public static Mat bufferedImageToMat(BufferedImage bi) {
        Mat mat = new Mat(bi.getHeight(), bi.getWidth(), CvType.CV_8UC3);
        Mat matDst = new Mat(bi.getHeight(), bi.getWidth(), CvType.CV_8UC1);
        byte[] data = ((DataBufferByte) bi.getRaster().getDataBuffer()).getData();
        mat.put(0, 0, data);
        Imgproc.cvtColor(mat, matDst, Imgproc.COLOR_RGB2GRAY);
        return matDst;
    }

    private static double countRatioOfImage(final Mat image) {
        return image.height() / (double) image.width();
    }


}

